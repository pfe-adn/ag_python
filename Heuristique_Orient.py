import os.path, copy, time, random, glob,sys
sys.path.append('./Modules')
sys.setrecursionlimit(10000)
from tkinter import *
from random import randrange
from Bio import SeqIO
import statistics
from PALS import Pals
from dataclasses import dataclass
from Dictionnaire_Orientation import *
from Fonctions_Heuristique import *
from Fonctions_Meta_Orient import *

start_time = time.time()

def TrouveFrag(d1,d2,d3,d4, Individual):  
        """ Trouver les fragments avec un chevauchement > seuil""" 
        l=list()  
        for i in Individual:
            BonfChevauchments = False
            for j in Individual:
                if i.Id_Frag==j.Id_Frag:
                    continue
                else:
                    if calcul_chevauch_heur(i,j,d1,d2,d3,d4) > 10:#10 est le seuil de chevauchement 
                        BonfChevauchments = True
            if  BonfChevauchments:
              l.append(i)
        return l

def TrouveOrdre(first,d1,d2,d3,d4,Individual, Fragments_Inverse):
    f=first
    Id=first.Id_Frag
    orient=first.Orientation
    ordre=[first]
    i=0

    max_chev=max(max(d1[Id].values()), max(d1[Id].values()))
    tmp=ValeurMax(d1[Id],d1[Id])

    while((max_chev >= 10) and (tmp[0] != Id)):
        Id=tmp[0]
        Orient=tmp[1]
        tmp_frag=Fragment(Id, '',Orient)
        nextRead=Create_Sequence(tmp_frag, Individual, Fragments_Inverse)
        ordre.append(nextRead)
        #peut etre ajouter des conditions pour l'orientation    
        max_chev=max(max(d1[Id].values()), max(d1[Id].values()))    
        tmp=ValeurMax(d1[Id], d1[Id])

    return ordre

def calcul_chevauch_heur(frag_1, frag_2, D_suff_pref, D_suff_suff, D_pref_pref, D_pref_suff):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation
    Orientation=1 : de Gauche --> Droite
    Orientation=0 : de Droite --> Gauche"""
    if frag_1.Orientation==1 and frag_2.Orientation==1:
        return D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==1 and frag_2.Orientation==0:
        return D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==0 and frag_2.Orientation==1:
        return D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==0 and frag_2.Orientation==0:
        return D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]

def Heuristique_Orient(d1,d2,d3,d4, Individual, Fragments_Inverse):
    premierEl=TrouveFrag(d1,d2,d3,d4,Individual)
    ListFrag=[]
    List_ordre=[[]] 
    for i in premierEl: 
        order=TrouveOrdre_mutation(i,d1,d2,d3,d4,Individual,Fragments_Inverse)
        List_ordre.append(order)
        #consensus=Assmblage(order,dict_meta, dict_chev)
        #tailleDeConsensus=len(consensus)
        #ListFrag.append(consensus)
    meill_ordre= sorted(List_ordre,key=lambda x: len(x))[-1]
    return meill_ordre
    """
    for element in Individual:
        if element not in meill_ordre:
            meill_ordre.append(element) """

def Heuristique_Mutation(indice_depart,d1,d2,d3,d4, Individual, Fragments_Inverse):
    """On suprimme les elements de contig resultant de l'individu, 
    aprés on l'ajoute a la fin de l'individu"""
    if type(indice_depart) is Fragment:
        """Si l'indice est un fragment, on trouve son chevauchment aprés on l'ajoute
        a la fin de l'individu et on le retourne"""
        contig=TrouveOrdre_mutation(indice_depart,d1,d2,d3,d4,Individual,Fragments_Inverse)
        Rest=[]
        Res_ids=[x.Id_Frag for x in contig]
        for x in Individual:
            if x.Id_Frag not in Res_ids:
                Rest.append(x)
        Rest.extend(contig)
        return Rest
        #contig.extend(Rest)
        #return contig
    else:
        if type(indice_depart) is list: 
            """si l'indice est un contig, on trouve le chevauchement de son dernier fragment
            aprés on ajoute le resultat a la fin du contig, aprés on ajoute le contig obtenu 
            a la fin de l'individu"""
            contig=TrouveOrdre_mutation(indice_depart[-1],d1,d2,d3,d4,Individual,Fragments_Inverse)
            indice_depart.extend(contig[1:])
            Rest=[]
            Res_ids=[x.Id_Frag for x in indice_depart]
            for x in Individual:
                if x.Id_Frag not in Res_ids:
                    Rest.append(x)
            #Rest.extend(indice_depart)
            #return Rest
            indice_depart.extend(Rest)
            return indice_depart
    return Individual

def TrouveOrdre_mutation(first,d1,d2,d3,d4,Individual, Fragments_Inverse):
    f=first
    Id=first.Id_Frag
    orient=first.Orientation
    ordre=[first]
    i=0
    """si l'orientation de premier element est 1, on cherche seulement dans
    les matrices dont l'orientation du fragment left est de gauche->droite
    """
    if orient==1:
        max_chev=max(max(d1[Id].values()), max(d2[Id].values()))
    """Sinon les 2 autres matrices"""
    if orient==0:
        max_chev=max(max(d3[Id].values()), max(d4[Id].values()))


    tmp=ValeurMax_mutation(first,d1[Id],d2[Id],d3[Id],d4[Id])

    while((max_chev >= 10) and (tmp[0] != Id)):
        Id=tmp[0]
        Orient=tmp[1]
        tmp_frag=Fragment(Id, '',Orient)
        nextRead=Create_Sequence(tmp_frag, Individual, Fragments_Inverse)
        ordre.append(nextRead)
        #peut etre ajouter des conditions pour l'orientation    
        if orient==1:
            max_chev=max(max(d1[Id].values()), max(d2[Id].values()))
        if orient==0:
            max_chev=max(max(d3[Id].values()), max(d4[Id].values()))
        tmp=ValeurMax_mutation(nextRead,d1[Id],d2[Id],d3[Id],d4[Id])

    return ordre

filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
fileslim=filelist[1]
filename= os.path.split(fileslim)[1]
dict_new={}
sequences=[]
ids=[]
Fragments=[]
Fragments_Inverse=[]
taille_ADN=0
for record in SeqIO.parse(fileslim, "fasta"):
    index = record.id.split('.')
    dict_new.update({(record.id): str(record.seq)})
    sequences.append(str(record.seq))
    ids.append(record.id)
    Frag_tmp=Fragment(record.id, str(record.seq), 1)
    Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(), 0)
    """on remplie les lists des fragments"""
    Fragments.append(Frag_tmp)
    Fragments_Inverse.append(Frag_tmp_inverse)
    taille_ADN+=len(str(record.seq))

D_suff_pref=Chevauch_Orientation(Fragments, Fragments)
D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)
D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)
D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)




if __name__ == '__main__':

    a=Heuristique_Orient(D_suff_pref,D_suff_suff,D_pref_pref,D_pref_suff,Fragments, Fragments_Inverse)
    print(len(a))
    print("\nTemps d execution Heuristique: %.2f secondes --" % (time.time() - start_time))    


