import os.path, copy, time, random, glob,sys
sys.path.append('./Modules')
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO
from PALS import Pals
#from dnaassemblyMAX import *
from Fonctions_Meta_Orient import *
from Dictionnaire_Orientation import *
from Heuristique_Orient import Heuristique_Orient, Heuristique_Mutation

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse

main_start_time = time.time()

def create_individu(Fragments): 
    """on utilise le résultat de l'heuristique pour créer les individus"""
    """sol=Res_heuristique[:]
    part_2=Rempli_Aleatoire(Rest)
    sol.extend(part_2)"""
    """on genere des individus completement aléatoirement """
    rand_choix=random.randrange(0,100)
    if rand_choix>0.2:
        sol=[]
        listFragsTmp=Fragments[:]
        for y in range(len(listFragsTmp)):
            val=random.choice(listFragsTmp)
            listFragsTmp.remove(val)
            val.Orientation=random.randint(0, 1)
            if val.Orientation==0:
                val.Seq_Frag=val.Inverse_Sequence()
            sol.append(val)
        return sol
    else:
        individual=Fragments[:] 
        #random.shuffle(individual)
        morceaux=Decoupe_individual(individual)
        print(morceaux)
        tmp_res=[]
        for i in range(len(morceaux)-1):
            ids_morc=[elm.Id_Frag for elm in\
            individual[morceaux[i]:morceaux[i+1]]] #ids

            seqs_morc=[dict_new[elm] for elm in ids_morc] #sequences
            tmp_res.extend(Pals(seqs_morc,ids_morc,morceaux[i]))
            time.sleep(1)
        sol=[]
        for element in tmp_res:
            sol.append(Fragment(element,dict_new[element],1))
        return sol
        
def Create_Sequence(Fragment):
    if Fragment.Orientation==1:
        Fragment.Seq_Frag=Fragments[Fragment.Id_Frag].Seq_Frag
    else:
        Fragment.Seq_Frag=Fragments_Inverse[Fragment.Id_Frag].Seq_Frag
    return Fragment

def calcul_chevauch(frag_1, frag_2):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation
    Orientation=1 : de Gauche --> Droite
    Orientation=0 : de Droite --> Gauche"""
    number=[D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag], \
               D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag],\
               D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag],\
               D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]]
    return max(number)

def inverse_orientation(Frag):
    Fragment=copy.deepcopy(Frag)
    Fragment.Seq_Frag= Fragment.Inverse_Sequence()
    if Fragment.Orientation==1:
        Fragment.Orientation=0
    else:
        Fragment.Orientation=1
    return Fragment

def swap_Orient(a, b):
    """ Fonction de permutation"""
    c=copy.deepcopy(a)
    d=copy.deepcopy(b)
    return inverse_orientation(d), inverse_orientation(c)

def Decoupe_individual(individual):
    """Decoupe l'individuel en morceaux de 50 fragments et retourne 
    une liste qui contient les indices des morceaux
    exemple: taille : 150
            --> resultat [0,50,100,150]
    on va l'utiliser dans mutation pour executer pals sur ces petits morceaux"""
    resultat=[0]
    length=len(individual)
    nb_divisions, reste= divmod(length, 100)

    if nb_divisions>0:
        for i in range(1,nb_divisions+1):
            resultat.append(100*i)

    if reste >= 20:
        resultat.append(reste+resultat[-1]+1)
    elif reste>0:
        if len(resultat)>1:
            resultat[-1]+=reste+1
        else:
            resultat.append(reste+resultat[-1]+1)
    elif reste<=0:
            resultat[-1]+=1

    return resultat

def mutation_Orient(individual):
    """ Swaper entre 2 fragments 
    On choisi l'index de l'individu a muté aleatoirement"""
    mutate_index = random.randrange(len(individual))
    mutate_index2= mutate_index
    while(mutate_index==mutate_index2):
        mutate_index2 = random.randrange(len(individual))
    individual[mutate_index],individual[mutate_index2]=\
    swap_Orient(individual[mutate_index],individual[mutate_index2])
    
    
    """
    mutate_index = random.choice(individual)
    Heuristique_Mutation(mutate_index,D_suff_pref,D_suff_suff\
                                        ,D_pref_pref,D_pref_suff, individual, Fragments_Inverse)
    """
    """
    morceaux=Decoupe_individual(individual)
    tmp_res=[]
    for i in range(len(morceaux)-1):
        ids_morc=[elm.Id_Frag for elm in\
                    individual[morceaux[i]:morceaux[i+1]]] #ids

        seqs_morc=[dict_new[elm] for elm in ids_morc] #sequences
        tmp_res.extend(Pals(seqs_morc,ids_morc))
        
    individual=tmp_res[:]
    """



def crossover_Orient(parent1,parent2):
    """Croisement a un point des deux parents"""       
    l = len(parent1)
    a = int(l / 2)
    p1 = parent1[0:a]
    p1_ids= [element.Id_Frag for element in p1]
    p2 = parent2[a:l]
    choices=[]

    for i in range(len(parent2)):
        """les fragments dans parent2 et non pas dans p1"""
        if parent2[i].Id_Frag not in p1_ids:
             choices.append(parent2[i])
             
    i=0
    for j in range(len(p2)):
        """Verifier si la valeur existe dans le premier parent"""
        if p2[j].Id_Frag in p1_ids:
           p2[j]=choices[i]
           i=i+1

    child1 = p1 + p2
    child2 = p2 + p1

    return child1,child2

def Fitness_Chevauchements(individual, Fragments): 
    """Fonction fitness qui maximise le nombre de chevauchement"""
    print("OK!")
    fitness = 0
    for i in range (len(individual)-1):
        fitness+=calcul_chevauch(individual[i], individual[i+1])
    """fitness = len(individual[0].Seq_Frag)
    for i in range (len(individual)-1):
        tmp_fitt=calcul_chevauch(individual[i], individual[i+1])
        if tmp_fitt>=10:
            fitness+=len(individual[i+1].Seq_Frag) """           
    return fitness

def create_contigs_Orient(individual):
    """Genere les contigs a partir d'un individuel et retourne """
    cpt=0
    contigs=[[individual[0]]]
    contigs_ids=[[individual[0].Id_Frag]]
    for i in range (len(individual)-1):
        
        #10: Seuil. 
        if (calcul_chevauch(individual[i], individual[i+1]) < 10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            contigs_ids.append([])
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(individual[i+1])
            contigs_ids[cpt].append(individual[i+1].Id_Frag)

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(individual[i+1]) 
            contigs_ids[cpt].append(individual[i+1].Id_Frag)


    return contigs,contigs_ids

def fitnessNbContig_Orient(individual, affich=1):
    """créer une liste des listes des fragments qui se chevauchent(contigs)"""
    tmp_contigs=create_contigs_Orient(individual)
    contigs=tmp_contigs[0]
    ids_contigs=sorted(tmp_contigs[1],key=lambda x: len(x))

    #determiner la taille de plus grand contig
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #les sequences de chaque contig:
    contigs_ordre_Asc=sorted(contigs,key=lambda x: len(x))
    sequences_contigs=[]
    frags_contigs=[]
    for element in contigs_ordre_Asc:
        if len(element)>=2:
            sequences_contigs.append(genere_seq_Orient(element))
            frags_contigs.append(element)

    #Determiner le meilleur contig
    meilleur_contig_ = meilleur_contig_Orient(contigs)[0]
    #la sequence des bases de meilleur contig
    meilleur_chev=meilleur_contig_Orient(contigs)[1]

    if affich:
        print("\nnombre de contigs final: ", len(contigs),\
        "\n\ntaille best contig: ", tailleBestContig,"\n\n")

    return len(contigs), tailleBestContig,meilleur_chev,sequences_contigs, frags_contigs, ids_contigs

list_generations=[70]
#f = open("Resultats/3.Stats_Meta_Orient_Aleatoire.csv","a+")
#f = open("Resultats/4.Stats_Meta_Orient_Hybrid.csv","a+")
filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))

for fileslim in filelist[4:5]:
    #filelist=sorted(glob.glob('./Programme C/Data/*.dat'))
    #fileslim=filelist[0]
    filename= os.path.split(fileslim)[1]
    dict_new={}
    sequences=[]
    ids=[]
    Fragments=[]
    Fragments_Inverse=[]
    taille_ADN=0
    counter=0
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        text="frag"+"{0:04d}".format(counter)   
        dict_new.update({str(text): str(record.seq)})
        sequences.append(str(record.seq))
        ids.append(text)
        Frag_tmp=Fragment(str(text), str(record.seq), 1)
        Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(), 0)
        """on remplie les lists des fragments"""
        Fragments.append(Frag_tmp)
        Fragments_Inverse.append(Frag_tmp_inverse)
        taille_ADN+=len(str(record.seq))
        counter+=1


    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de gauche -> droite.
        L'orientation des fragments 'Right' est de gauche -> droite. """
    D_suff_pref=Chevauch_Orientation(Fragments, Fragments)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de gauche -> droite. 
        L'orientation des fragments 'Right' est de droite -> gauche."""
    D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de droite -> gauche. 
        L'orientation des fragments 'Right' est de gauche -> droite."""
    D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de droite -> gauche. 
        L'orientation des fragments 'Right' est de droite -> gauche."""
    D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)
    """
    Res_heuristique=Heuristique_Orient(D_suff_pref,D_suff_suff,\
            D_pref_pref,D_pref_suff,Fragments,Fragments_Inverse)
    print("Resultat de l'heuristique: ",len(Res_heuristique))


    Rest=[]
    Res_ids=[x.Id_Frag for x in Res_heuristique]
    for x in Fragments:
        if x.Id_Frag not in Res_ids:
            Rest.append(x)
    """
    print("\nFichier: %s" % filename,
          "\nInfos initiales:\n\
            Taille totale des Bases: %d\n\
            Nombre de contigs: %d\n\
            Taille de meilleur contig: %d"% (taille_ADN,len(ids),1))
    
    """f.write("\t\tFichier: %s\t\t\t\n"% filename)
    f.write("\n\tNombre de chevauchments, nombre de contigs, taille de meilleur contig")
    f.write("\nInformations Initiales: ,%s"% str(taille_ADN)+","+ str(len(ids))+","+ str(1))
    """  

    for nb_gens in list_generations:
        temps_init=time.time()
        ga = pyeasyga.GeneticAlgorithm (Fragments,
                                   population_size=800,
                                   generations=nb_gens,
                                   crossover_probability=0.5,
                                   mutation_probability=0.5,
                                   elitism=True,
                                   maximise_fitness=True) 
        ga.create_individual=create_individu
        ga.fitness_function=Fitness_Chevauchements
        ga.crossover_function=crossover_Orient
        ga.mutate_function=mutation_Orient
        ga.selection=ga.tournament_selection 
        ga.run()
        best=ga.best_individual()
        last_gen=ga.last_generation()

        #on recupère le tuple (nbr de chevauchments, individu) pour chaque individu de la population
        #informations_population=infos_population(last_gen)
        #Pop_stat=population_stats(informations_population)
        #pourcentage= "%.2f%%" % (int(Pop_stat[0][0])*100/taille_ADN)
        #print("Meilleur Individu: ",Pop_stat[0],
        #    "\nPourcentage chevauché ac succés: ",pourcentage)

        #print("Taille de best contig de l'heuristique: ", len(Res_heuristique))
        #print("Taille de best contig de la Meta Heuristique: ", Pop_stat[0][2])


        #on recupère: [Nbr chevauchments, Nbr contigs, taillebestcontig] pour chaque individu
        #statistiques_population=population_stats(informations_population)
        fit=fitnessNbContig_Orient(best[1],0)
        pourcentage= "%.2f%%" % (best[0]*100/taille_ADN)

        print("\nMeilleur Individu:\n\
        Taille de chevauchement: %d\n\
        Nombre de contigs: %d\n\
        Taille de meilleur contig: %d"% (best[0], fit[0], fit[1]))
    
        print("\nPourcentage chevauché ac succés: ",pourcentage)

        """ Affichage des bases des contigs qui ont une taille > 1 (plus d'un fragment)
        seqs_contigs=fit[3]
        tailles_seqs_contigs=[]
        for element in seqs_contigs:
            tailles_seqs_contigs.append(len(element))

        print("\nLes longueurs des bases des contigs (taille > 1) de meilleur individu: ")
        print(tailles_seqs_contigs)""" 

        #Affichage des fragments des contigs qui contiennent plus d'un fragment
        
        frags_contigs=fit[4]
        taille_frags_contigs=[]
        ids_frags=[]

        for element in frags_contigs:
            taille_frags_contigs.append(len(element))
            tmp=[]
            for i in element:
                tmp.append(i.Id_Frag)
            ids_frags.append(tmp)

        print("\nLes tailles contigs (taille > 1) de meilleur individu: ")
        #print(ids_frags)
        print(taille_frags_contigs)
        print(fit[5])

        
        temps_exec=time.time() - temps_init
        """
        #On rempli le fichier csv:
        #f.write("\t\tFichier: %s\t\t\t\n"% filename)
        f.write("\n\n\t\tNombre de generations: %d\t\t\t"% nb_gens)
        #f.write("\n\tNombre de chevauchments, nombre de contigs, taille de meilleur contig")
        #f.write("\nInformations Initiales: ,%s"% str(taille_ADN)+","+ str(len(ids))+","+ str(1))
        f.write("\nMeilleur individu:, %s" % (",".join((str(best[0]), str(fit[0]), str(fit[1])))))
        f.write("\nPourcentage chevauché: %s" % pourcentage)
        f.write("\nTemps d'execution: %.2f sec" % temps_exec)
    f.write("\n\n\n\n")


f.close()"""
print("\nTemps d execution : %.2f secondes --" % (time.time() - main_start_time))    






"""
#Pour Pals on donne une liste qui contient les sequences et une liste qui contient leur ordre
Resultat=Pals(Gen_list_Seq(ordre),ordre)
print("\n",Resultat)
"""
