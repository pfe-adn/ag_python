import os.path, copy, time, random, glob,sys
sys.path.append('../Modules')
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO
from PALS import Pals
from dnaassemblyMAX import *
from Fonctions_Meta import *

# Debut du decompte du temps
start_time = time.time()

"""Traitment de fichier de données"""
filelist=sorted(glob.glob('../Programme C/dna_instances_ordered/*.dat'))
#fileslim=filelist[2]
#fileslim=filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.dat"),("all files","*.*")))



def create_individu(data):
    """Creation de l'individu en aleatoire"""
    
    #Creation de l'individu en aleatoire
    rand_choix=random.randrange(0,300)
    if rand_choix<=100:
        sol=[]
        listFragsTmp=[]
        for x in data:
            listFragsTmp.append(x)
        for y in range(len(data)):
            val=random.choice(listFragsTmp)
            listFragsTmp.remove(val)
            sol.append(val)
        return sol


    elif 100<rand_choix<=200:
        #on initialise l'individu avec le resultat de l'heuristique
        individu=Res_heuristique[:]
        random.shuffle(individu)
        #ensuite on ajoute les fragments restant de manière aléatoire
        part_2=Rempli_Aleatoire(Rest)
        individu.extend(part_2)
        return individu

    else:
        #on initialise l'individu avec le rest d'abord
        part_1=Rempli_Aleatoire(Rest)
        individu=part_1[:]
        #ensuite on ajoute le resultat de l'heuristique
        individu.extend(Res_heuristique)
        return individu

def calcul_chevauch(frag_1, frag_2):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation"""
    number=[D_suff_pref[frag_1][frag_2], \
               D_suff_suff[frag_1][frag_2],\
               D_pref_pref[frag_1][frag_2],\
               D_pref_suff[frag_1][frag_2]]
    return max(number)

def fitMaxChevauchements( individual , data ): 
    """Fonction fitness qui maximise le nombre de chevauchement"""
    fitness = 0
    """for i in range (len(individual)-1):
        fitness+=calcul_chevauch(individual[i], individual[i+1])"""
    fitness = len(dict_new[individual[0]])
    for i in range (len(individual)-1):
        tmp_fitt=calcul_chevauch(individual[i], individual[i+1])
        if tmp_fitt>=10:
            fitness+=len(dict_new[individual[i+1]])
    return fitness


def crossover(parent1,parent2):
    """Croisement a un point des deux parents"""       
    l = len(parent1)
    a = int(l / 2)
    p1 = parent1[0:a]
    p2 = parent2[a:l]
    choices=[element for element in parent2[0:a] if element not in p1]
    indices=[p2.index(element) for element in p2 if element in p1]
    i=0                         
    for indice in indices:
        p2[indice]=choices[i]
        i=i+1
 
    return p1 + p2,p2 + p1
    
def mutation(individual):
    """ Executer pals sur l'individu 
    || Swaper entre 2 fragments
    """
    """On choisi l'index de l'individu a muté aleatoirement"""
    mutate_index = random.randrange(len(individual))
    mutate_index2= mutate_index
    while(mutate_index==mutate_index2):
        mutate_index2 = random.randrange(len(individual))
    individual[mutate_index],individual[mutate_index2]=swap(individual[mutate_index],individual[mutate_index2])



def create_contigs(individual):
    """Genere les contigs a partir d'un individuel et retourne """
    cpt=0
    contigs=[[individual[0]]]
    for i in range (len(individual)-1):
        
        #10: Seuil. 
        if (calcul_chevauch(individual[i], individual[i+1]) < 10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(individual[i+1])

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(individual[i+1])  

    return contigs

def fitnessNbContig(individual, d, affich):
    #créer une liste des listes des fragments qui se chevauchent(contigs)
    contigs=create_contigs(individual)

    #determiner la taille de plus grand contig
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #Determiner le meilleur contig
    meilleur_contig_ = meilleur_contig(contigs,dict_new)

    if affich:
        print("\nnombre de contigs final: ", len(contigs),\
        "\n\ntaille best contig: ", tailleBestContig,"\n\n")

    return len(contigs), tailleBestContig 

""" Programme principal"""

"""On execute l'algorithme genetic pour des differents nombres de generations et on analyse:"""
list_generations=[55]
nb_gens=60

f = open("Resultats/4.Stats..AG_NvFitness+Heuristique.csv","a+")

"""filelist contient les noms de fichiers adn en ordre croissant par nombre des fragments
ici, on parcour juste les 4 premiers fichiers qui sont les plus petits"""
for fileslim in filelist[9:10]:
    filename= os.path.split(fileslim)[1]
    dict_new={}
    dict_inverse={}
    records = list(SeqIO.parse(fileslim, "fasta"))
    data=[]
    taille_ADN=0
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        dict_new.update({(record.id): str(record.seq)})
        #dictionnaire qui contient les sequences inverse
        Seq_Inverse=''.join(reversed(str(record.seq)))
        dict_inverse.update({(record.id): Seq_Inverse})
        data.append(record.id)
        taille_ADN+=len(str(record.seq))


    d = ChevechTousOLCGRE(dict_new)

    D_suff_pref=Chevauch_Orientation(dict_new, dict_new)
    D_suff_suff=Chevauch_Orientation(dict_new, dict_inverse)
    D_pref_pref=Chevauch_Orientation(dict_inverse, dict_new)
    D_pref_suff=Chevauch_Orientation(dict_inverse, dict_inverse)
    

    """
    On utilise le resultat de l'heuristique pour créer les individus et commencer avec 
    le plus grand contig puis les autres fragments on les rempli aléatoirment   """
    temp_dict=copy.copy(d)  
    Res_heuristique=Heuristique(temp_dict,dict_new)

    
    print("\nFichier: %s" % filename,
          "\nInfos initiales:\n\
            Taille totale des Bases: %d\n\
            Nombre de contigs: %d\n\
            Taille de meilleur contig: %d"% (taille_ADN,len(data),1))
     
    
    Rest=[]
    for x in data:
            if x not in Res_heuristique:
                Rest.append(x)
    
    f.write("\t\tFichier: %s\t\t\t"% filename)
    f.write("\n\t\tNombre de generations: %d\t\t\t\n"% nb_gens)
    f.write("\nInformations Initiales: ,%s"% \
        "Taille totale de séquences: "+str(taille_ADN)+","+ "Nombre de fragments: "+str(len(data)))
    f.write("\n\n\tNombre de chevauchments, Nombre de contigs, Taille de Meilleur contig,Pourcentage Chevauché,Temps d'exécution")

    for iterator in range(3):
        temps_init=time.time()
        ga = pyeasyga.GeneticAlgorithm (data,
                                   population_size=800,
                                   generations=nb_gens,
                                   crossover_probability=0.8,
                                   mutation_probability=0.8,
                                   elitism=True,
                                   maximise_fitness=True) 
        ga.create_individual=create_individu
        ga.fitness_function=fitMaxChevauchements
        ga.crossover_function=crossover
        ga.mutate_function=mutation
        ga.selection=ga.tournament_selection 
        ga.run()
        best=ga.best_individual()
        last_gen=ga.last_generation()

        taille_chevauch=0
        for i in range(len(best[1])-1):
            taille_chevauch+=calcul_chevauch(best[1][i], best[1][i+1])



        #on recupère le tuple (nbr de chevauchments, individu) pour chaque individu de la population
        informations_population=infos_population(last_gen)
        pourcentage= "%.2f%%" % (taille_chevauch*100/taille_ADN)
        fit=fitnessNbContig(best[1],d,0)

        print("\nMeilleur Individu:\n\
        Taille de chevauchement: %d\n\
        Nombre de contigs: %d\n\
        Taille de meilleur contig: %d"% (taille_chevauch, fit[0], fit[1]))
    
        print("\nPourcentage chevauché ac succés: ",pourcentage)
        


        temps_exec=time.time() - temps_init
        
        #On rempli le fichier csv:
        f.write("\nTest N°%d:"% iterator) 
        f.write("\t%s" % (",".join((str(taille_chevauch), str(fit[0]), str(fit[1]),str(pourcentage)))))
        f.write("\t%.2f" % temps_exec)
    f.write("\n\n")
    time.sleep(15)
f.close()

# Affichage du temps d execution
print("\nTemps d execution : %.2f secondes ---" % (time.time() - start_time))    