import os.path, copy, time, random, glob, sys
sys.path.append("../Modules")
sys.path.append('../PALS Python')
from dataclasses import dataclass
from Bio import SeqIO
from Fonctions_pals import *
start_time = time.time()

filelist=sorted(glob.glob('../Programme C/dna_instances_ordered/*.dat'))
f = open("Resultats/5.Stats..PALS.csv","a+")

"""filelist contient les noms de fichiers adn en ordre croissant par nombre des fragments
ici, on parcour juste les 4 premiers fichiers qui sont les plus petits"""
for fileslim in filelist[9:10]:
    filename= os.path.split(fileslim)[1]
    dict_new={}
    sequences=[]
    ids=[]
    Fragments=[]
    Fragments_Inverse=[]
    taille_ADN=0
    counter=0
    tmp_overlap={1:0, 0:0}
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        text="frag"+"{0:04d}".format(counter)  
        dict_new.update({str(text): str(record.seq)}) 
        sequences.append(str(record.seq))
        ids.append(text)
        Frag_tmp=Fragment(str(text), str(record.seq),tmp_overlap, 1)
        Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(),tmp_overlap, 0)
        Fragments.append(Frag_tmp)
        Fragments_Inverse.append(Frag_tmp_inverse)
        taille_ADN+=len(str(record.seq))
        counter+=1

    D_suff_pref=Chevauch_Orientation(Fragments, Fragments)
    D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)
    D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)
    D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)

    print("\nFichier: %s" % filename,
          "\nInfos initiales:\n\
            Taille totale des Bases: %d\n\
            Nombre de contigs: %d\n\
            Taille de meilleur contig: %d"% (taille_ADN,len(ids),1))

    f.write("\t\tFichier: %s\t\t\t"% filename)
    f.write("\nInformations Initiales: ,%s"% \
        "Taille totale de séquences: "+str(taille_ADN)+","+ "Nombre de fragments: "+str(len(ids)))
    f.write("\n\n\tNombre de chevauchments, Nombre de contigs,Pourcentage Chevauché,Temps d'exécution")


    for iterator in range(4):
        temps_init=time.time()
        tmp=Fragments[:]
        Resultat=PALS((tmp), counter, D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
        individuel=Resultat[0]
        taille_chevauch=Resultat[1]
        #on recupère le tuple (nbr de chevauchments, individu) pour chaque individu de la population
        pourcentage= "%.2f%%" % (taille_chevauch*100/taille_ADN)

        print("\nMeilleur Individu:\n\
        Taille de chevauchement: %d\n\
        Nombre de contigs: %d"% (taille_chevauch, Resultat[2]))
    
        print("\nPourcentage chevauché ac succés: ",pourcentage)
        


        temps_exec=time.time() - temps_init
        
        #On rempli le fichier csv:
        f.write("\nTest N°%d:"% iterator) 
        f.write("\t%s" % (",".join((str(taille_chevauch), str(Resultat[2]),str(pourcentage)))))
        f.write("\t%.2f" % temps_exec)
    f.write("\n\n")
    time.sleep(10)
f.close()


print("\nTemps d execution : %.2f secondes ---" % (time.time() - start_time))    