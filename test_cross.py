import random
def swap(a, b):
    """ Fonction de permutation"""
    return b, a

def crossover(parent1,parent2):
    """Nouveau Crossover"""
    l = len(parent1)
    point1=random.randrange(0, int(l/2))
    point2=random.randrange(int(l/2),l+1)
    #Les parties de milieu
    p2=parent2[point1:point2]
    p1=parent1[point1:point2]
    #Les restes de P1 et P2
    ResteP1=parent1[0:point1]+parent1[point2:]
    ResteP2=parent2[0:point1]+parent2[point2:]
    indices1=[ResteP1.index(element) for element in ResteP1 if element in p2]
    indices2=[ResteP2.index(element) for element in ResteP2 if element in p1]
    i=0                         
    for indice in indices1:
        ResteP1[indice], ResteP2[indices2[i]]=swap(ResteP1[indice], ResteP2[indices2[i]])
        i=i+1
    
    p11=ResteP1[0:point1]
    p12=ResteP1[point1:]
    p21=ResteP2[0:point1]
    p22=ResteP2[point1:]
    return p11 + p2 + p12, p21 + p1 + p22

A=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
B=[20,5,16,3,4,11,12,10,6,7,8,9,2,1,15,19,17,18,13,14]


