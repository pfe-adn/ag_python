import os.path, copy, time, random, glob, sys
sys.path.append('../Modules')
from Dictionnaire_Orientation import *
from dataclasses import dataclass
from collections import defaultdict

PALS_CUTOFF = 50

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    #1: chevauchement a gauche de fragment, 0: chevauchement a droite
    overlap: dict
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse

    def getsequence(self):
        if self.Orientation==1:
            return self.Seq_Frag
        else:
            Inverse=''.join(reversed(self.Seq_Frag))
            return Inverse


@dataclass
class CandidateMovement:
    i: int
    j: int
    iFitness: int
    jFitness: int
    fitnessVariation: int
    contigVariation: int

@dataclass
class Movement:
    i: int
    j: int
    iFitness: int 
    jFitness: int

@dataclass
class Contig:
    length: int
    sequence: str
    coverage: list

#Create_fragment pas necessaire parceque on le fait deja.
def copyFragment(Frag, Reverse_orient):
    Id=Frag.Id_Frag

    if Reverse_orient:
        seq=Frag.Inverse_Sequence()
        orient=Frag.Inverse_Orient()
    else:
        seq=Frag.Seq_Frag
        orient=Frag.Orientation

    overlap[1]=Frag.overlap[1]
    overlap[0]=Frag.overlap[0]

    f=Fragment(Id, seq, orient, overlap)
    return f

#Destroy_Fragment n'est pas necessaire
def swapFragments(individual,i,j):
    """ Fonction de permutation"""
    c=copy.deepcopy(individual[i])
    d=copy.deepcopy(individual[j])
    individual[j]=c
    individual[i]=d

def flipFragment(Fragment):
    if Fragment.Orientation==1:
        Fragment.Orientation= 0
    else:
        Fragment.Orientation= 1



def reverseObjective(candidateMovement):
    candidateMovement.contigVariation = - candidateMovement.contigVariation
    candidateMovement.fitnessVariation = - candidateMovement.fitnessVariation
    
def baseAt(Fragment, b):
    return Fragment.Seq_Frag[b]

def calc_overlaps(frag_1, frag_2,overlapScore, D_suff_pref, D_suff_suff, D_pref_pref, D_pref_suff):
    """Vérifier si le suffixe du fragment <f> et le préfixe du fragment <g>
    se chevauchent sur une longueur <overlapScore>"""
    if frag_1.Orientation==1 and frag_2.Orientation==1:
        if D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag] >= overlapScore:
            return D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    elif frag_1.Orientation==1 and frag_2.Orientation==0:
        if D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag] >= overlapScore:
            return D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag]

    elif frag_1.Orientation==0 and frag_2.Orientation==1:
        if D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag] >= overlapScore:
            return D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    elif frag_1.Orientation==0 and frag_2.Orientation==0:
        if D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag] >= overlapScore:
            return D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]
    return 0


""" Récupérer après calcul le chevauchement maximal entre le suffixe 
de <f> et le préfixe de <g> en partant de <initialOverlap> (par défaut <initialOverlapScore = 0>)"""
def fitness_ancien(f, g, initialOverlapScore,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):
    """
    bestOverlapScore = 0 # Valeur du meilleur chevauchement entre <f> et <g> (initialement 0)
    # D'abord on cherche un chevauchement meilleur que initialOverlapScore
    bestOverlapScore = calc_overlaps(f,g,initialOverlapScore,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    if bestOverlapScore == 0: # Si aucun chevauchement meilleur n'est trouvé
        bestOverlapScore=calc_overlaps(f,g,PALS_CUTOFF,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    """
    bestOverlapScore=calcul_chevauch(f,g,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    return bestOverlapScore

def fitness_ancien2(f, g, initialOverlapScore,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):
    bestOverlapScore = 0 # Valeur du meilleur chevauchement entre <f> et <g> (initialement 0)
    # D'abord on cherche un chevauchement meilleur que initialOverlapScore
    bestOverlapScore = calc_overlaps(f,g,initialOverlapScore,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    if bestOverlapScore == 0: # Si aucun chevauchement meilleur n'est trouvé
        bestOverlapScore=calc_overlaps(f,g,PALS_CUTOFF,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    return bestOverlapScore

def getOverlap(f, direction):
    tmp=f.Orientation ^ direction
    return f.overlap[tmp]

def setOverlap(f, direction, newOverlapScore):
    tmp=f.Orientation ^ direction
    f.overlap.update({tmp:newOverlapScore})


#Génération de la solution initiale (aléatoire)
def generateInitialSolution(individual, length,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):

    # Calcul des chevauchements initiaux
    for i in range(length-1):
        j = fitness_ancien(individual[i],individual[i+1],PALS_CUTOFF,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
        setOverlap(individual[i],0,j)
        setOverlap(individual[i+1],1,j)


    # Permutations aléatoires des fragments 
    for i in range(length):
        j = i + random.randrange(length-i)
        swapFragments(individual,i,j)

        k = random.randrange(1,4)

        if k== 1: flipFragment(individual[i])
        if k== 2: flipFragment(individual[j])
        if k== 3: 
            flipFragment(individual[i])
            flipFragment(individual[j])
        
        flipFragment(individual[i])
        flipFragment(individual[j])

# Calcul d'un mouvement candidat (i <= j)
def calculateDelta(fragmentVector,length, candidateMovement,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):

    i = candidateMovement.i
    j = candidateMovement.j
    fitnessVar = 0
    contigVar = 0
    tmpFitness = 0
    candidateMovement.iFitness = 0
    candidateMovement.jFitness = 0

    # Supprimession du chevauchement F[i-1]F[i]
    tmpFitness  = getOverlap(fragmentVector[i],1)
    if(tmpFitness >= PALS_CUTOFF):
        contigVar+=1
    fitnessVar -= tmpFitness

    # Ajout du chevauchement F[i-1]F[j]'
    if(i > 0):
        tmpFitness = fitness_ancien(fragmentVector[i-1],fragmentVector[j],max(tmpFitness,PALS_CUTOFF),D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
        if(tmpFitness >= PALS_CUTOFF):
            contigVar-=1
        candidateMovement.jFitness = tmpFitness
        fitnessVar += tmpFitness
    

    # Supprimession du chevauchement F[j]F[j+1]
    tmpFitness = getOverlap(fragmentVector[j],0)
    if(tmpFitness >= PALS_CUTOFF):
        contigVar+=1
    fitnessVar -= tmpFitness
    # Ajout du chevauchement F[i]'F[j+1]
    if(j<length-1):
    
        tmpFitness  = fitness_ancien(fragmentVector[i],fragmentVector[j+1],max(tmpFitness,PALS_CUTOFF),D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
        if(tmpFitness >= PALS_CUTOFF):
            contigVar-=1
        candidateMovement.iFitness = tmpFitness
        fitnessVar += tmpFitness
    

    # Mise à jours du mouvement candidat
    candidateMovement.fitnessVariation = fitnessVar
    candidateMovement.contigVariation = contigVar

# Sélection d'un mouvement parmi la liste des mouvements 
#candidats <candidateMovements>
def selectMovement(candidateMovements, cmLength):

    #int index  Variable de boucle pour parcourir les mouvements candidats
    selectedMovement = None # Mouvement à appliquer (initialement NULL)

    #s = sorted(sorted(candidateMovements, key = lambda x : x.fitnessVariation, reverse=True), key = lambda x : x.contigVariation)  
    s = sorted(candidateMovements, key = lambda x : x.contigVariation) 
    
    #on veut le mouvement avec la plus petite contigVariation
    if (s[0].contigVariation<0 or ((s[0].contigVariation==0) and s[0].fitnessVariation>0)):
        i = s[0].i
        j = s[0].j
        iFitness = s[0].iFitness
        jFitness = s[0].jFitness
        selectedMovement=Movement(i,j,iFitness,jFitness)

    return selectedMovement

#Application du mouvement <movement> à la solution <fragmentVector>
def applyMovement(fragmentVector,length, movement):
    i = movement.i
    j = movement.j
    # Mise à jour du chevauchement F[i]'F[j+1]
    setOverlap(fragmentVector[i],1,movement.iFitness)
    if(j<length-1):
        setOverlap(fragmentVector[j+1], 1, movement.iFitness)

    # Mise à jour du chevauchement F[i-1]F[j]'
    setOverlap(fragmentVector[j], 0, movement.jFitness)
    if(i>0):
        setOverlap(fragmentVector[i-1], 0, movement.jFitness)

    # Inversion de la sous-permutation des fragments de 
    #la solution compris entre <i> et <j> inclus
    while(i<j):
        swapFragments(fragmentVector,i,j)
        flipFragment(fragmentVector[i])
        flipFragment(fragmentVector[j])

        i+=1
        j-=1
    

    # Une dernière inversion si le nombre de fragments est impair
    if(i == j):
    
        flipFragment(fragmentVector[i])


def PALS(fragmentVector, length,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):
    mIndex = 0 # indice du mouvement candidat actuel
    solutionImproved  = 1 # Spécifie si la solution est améliorée
    movementsPerIteration = int((length+1)*(length/2)-1)  # Nombre de mouvements candidats à calculer par itération
    iterations = 0 # Nombre d'itérations
    candidateMovements = [CandidateMovement(0,0,0,0,0,0)] * movementsPerIteration #Liste des mouvements candidats
    nbr_contigs = 0

    generateInitialSolution(fragmentVector,length,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff) # Génération de la solution initiale
    for i in range(length):
        if(getOverlap(fragmentVector[i],1)<50):
            nbr_contigs+=1


    # Première itération ---------------------------------------------------------------
        # Nouvelle itération
    iterations+=1
    # Calcul des mouvements candidats (sans mouvements équivalents [i <= j])
    for j in range(length-1): # j : longueur de la sous-permutation - 1 [length - 1] car length est la longueur de la séquence
        i=0
        while(i+j<length):# i : indice de début de la sous permutation
            # for(i = 0; i+j < length; i++) 
            tmp_i = i
            tmp_j = i+j
            tmp_candidat=CandidateMovement(tmp_i, tmp_j, 0,0,0,0)
            calculateDelta(fragmentVector,length,tmp_candidat,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
            candidateMovements[mIndex]=tmp_candidat
            mIndex+=1
            i+=1

    # Selection d'un mouvement dans la liste candidateMovements
    selectedMovement = selectMovement(candidateMovements,movementsPerIteration)

    if(selectedMovement == None): # Si aucun mouvement n'est choisi
        solutionImproved = 0 # Plus aucune amélioration de la solution
        #print("\nAucun mouvement appliqué")
    
    else:
    
        #print("\nMouvement appliqu\x82 : <%d-%d>" % (selectedMovement.i,selectedMovement.j))
        applyMovement(fragmentVector,length,selectedMovement)


    #printf("\nfin de l'iteration %d\n",iterations);

    # Boucle principale -----------------------------------------------------------------
    while(solutionImproved):
    
        # Nouvelle itération
        iterations+=1
        mIndex = 0
        k = 0
        # Calcul des mouvements candidats (sans mouvements équivalents [i <= j])
        for j in range(length-1):
            i=0
            while(i+j<length):
                if((i == selectedMovement.i) and (j == selectedMovement.j)):
                    reverseObjective(candidateMovements[mIndex])

                elif ((i > selectedMovement.i) and (j < selectedMovement.j)):
                    calculateDelta(fragmentVector,length,candidateMovements[mIndex],D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
            
                elif ((i < selectedMovement.i) and (j > selectedMovement.j)):
                    pass

                else: 
                    calculateDelta(fragmentVector,length,candidateMovements[mIndex],D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff) 
                    k+=1
                mIndex+=1
                i+=1
            

        # Selection d'un mouvement dans la liste candidateMovements
        selectedMovement = selectMovement(candidateMovements,movementsPerIteration)


        if(selectedMovement == None): # Si aucun mouvement n'est choisi
            solutionImproved = 0 # Plus aucune amélioration de la solution
            #print("\nAucun mouvement appliqué")
        
        else:
        
            print("\nMouvement applique : <%d-%d>" % (selectedMovement.i,selectedMovement.j))
            applyMovement(fragmentVector,length,selectedMovement)
            """
            temp=Frags[selectedMovement->i];
            Frags[selectedMovement->i]=Frags[selectedMovement->j];
            Frags[selectedMovement->j]=temp;

            Orient[selectedMovement->i]=fragmentVector[selectedMovement->j]->orientation;
            Orient[selectedMovement->j]=fragmentVector[selectedMovement->i]->orientation;
            """
        
        #print("\nfin de l'iteration %d\n"%iterations)
    
    #while(solutionImproved); #// Tant qu'il y a une amélioration de la solution


    # printf("\nNombre d'it\x82rations : %d\n",iterations);

    print("\nNombre total de contigs de la solution initiale: %d" % nbr_contigs)
    
    contigs=[[fragmentVector[0]]]
    Ids_Fragments=[]
    cpt=0
    for i in range (len (fragmentVector)-1):
        #10: Seuil. 
        Ids_Fragments.append(fragmentVector[i].Id_Frag)
        if(calcul_chevauch(fragmentVector[i],fragmentVector[i+1],D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)<10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(fragmentVector[i+1])

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(fragmentVector[i+1])  
    print("\nNombre total de contigs de la solution finale: ", len(contigs))
    Ids_Fragments.append(fragmentVector[-1].Id_Frag)

    taille_chevauch=0
    for i in range(len(fragmentVector)-1):
        taille_chevauch+=calcul_chevauch(fragmentVector[i], fragmentVector[i+1],D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    return Ids_Fragments, taille_chevauch,len(contigs)

            
def calcul_chevauch(frag_1, frag_2,D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation
    Orientation=1 : de Gauche --> Droite
    Orientation=0 : de Droite --> Gauche"""
    number=[D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag], \
               D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag],\
               D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag],\
               D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]]
    return max(number)
