import os.path, copy, time, random, glob, sys
sys.path.append("../Modules")
from dataclasses import dataclass
from Bio import SeqIO
from Fonctions_pals import *
start_time = time.time()

filelist=sorted(glob.glob('../Programme C/dna_instances_ordered/*.dat'))

for fileslim in filelist[0:1]:
    #filelist=sorted(glob.glob('./Programme C/Data/*.dat'))
    #fileslim=filelist[0]
    filename= os.path.split(fileslim)[1]
    dict_new={}
    sequences=[]
    ids=[]
    Fragments=[]
    Fragments_Inverse=[]
    taille_ADN=0
    counter=0
    tmp_overlap={1:0, 0:0}
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        text="frag"+"{0:04d}".format(counter)   
        sequences.append(str(record.seq))
        ids.append(text)
        Frag_tmp=Fragment(str(text), str(record.seq),tmp_overlap, 1)
        Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(),tmp_overlap, 0)
        Fragments.append(Frag_tmp)
        Fragments_Inverse.append(Frag_tmp_inverse)
        taille_ADN+=len(str(record.seq))
        counter+=1

    D_suff_pref=Chevauch_Orientation(Fragments, Fragments)
    D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)
    D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)
    D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)

    Resultat=PALS(Fragments, counter, D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)
    individuel=Resultat[0]
    Chevauchement=Resultat[1]
    print(Chevauchement)
print("\nTemps d execution : %.2f secondes ---" % (time.time() - start_time))    