import os.path, copy, time, random, glob,sys
sys.path.append('./Modules')
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO
from PALS import Pals
#from dnaassemblyMAX import *
from Fonctions_Meta_Orient import *
from Dictionnaire_Orientation import *
from Heuristique_Orient import Heuristique_Orient, Heuristique_Mutation


@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse

main_start_time = time.time()


def Create_Sequence(Fragment):
    if Fragment.Orientation==1:
        Fragment.Seq_Frag=Fragments[Fragment.Id_Frag].Seq_Frag
    else:
        Fragment.Seq_Frag=Fragments_Inverse[Fragment.Id_Frag].Seq_Frag
    return Fragment

def calcul_chevauch(frag_1, frag_2):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation
    Orientation=1 : de Gauche --> Droite
    Orientation=0 : de Droite --> Gauche"""
    number=[D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag], \
    		   D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag],\
    		   D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag],\
			   D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]]
    return max(number)


def inverse_orientation(Frag):
    Fragment=copy.deepcopy(Frag)
    Fragment.Seq_Frag= Fragment.Inverse_Sequence()
    if Fragment.Orientation==1:
        Fragment.Orientation=0
    else:
        Fragment.Orientation=1
    return Fragment



def create_contigs_Orient(individual):
    """Genere les contigs a partir d'un individuel et retourne """
    cpt=0
    contigs=[[individual[0]]]
    contigs_ids=[[individual[0].Id_Frag]]
    for i in range (len(individual)-1):
        
        #10: Seuil. 
        if (calcul_chevauch(individual[i], individual[i+1]) < 10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            contigs_ids.append([])
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(individual[i+1])
            contigs_ids[cpt].append(individual[i+1].Id_Frag)

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(individual[i+1]) 
            contigs_ids[cpt].append(individual[i+1].Id_Frag)


    return contigs,contigs_ids

def fitnessNbContig_Orient(individual, affich=1):
    """créer une liste des listes des fragments qui se chevauchent(contigs)"""
    tmp_contigs=create_contigs_Orient(individual)
    contigs=tmp_contigs[0]
    ids_contigs=tmp_contigs[1]

    #determiner la taille de plus grand contig
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #les sequences de chaque contig:
    contigs_ordre_Asc=sorted(contigs,key=lambda x: len(x))
    sequences_contigs=[]
    frags_contigs=[]
    for element in contigs_ordre_Asc:
        if len(element)>=2:
            sequences_contigs.append(genere_seq_Orient(element))
            frags_contigs.append(element)

    #Determiner le meilleur contig
    meilleur_contig_ = meilleur_contig_Orient(contigs)[0]
    #la sequence des bases de meilleur contig
    meilleur_chev=meilleur_contig_Orient(contigs)[1]

    if affich:
        print("\nnombre de contigs final: ", len(contigs),\
        "\n\ntaille best contig: ", tailleBestContig,"\n\n")

    return len(contigs), tailleBestContig,meilleur_chev,sequences_contigs, frags_contigs, ids_contigs


filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))

for fileslim in filelist[0:1]:
    filename= os.path.split(fileslim)[1]
    dict_new={}
    sequences=[]
    ids=[]
    Fragments=[]
    Fragments_Inverse=[]
    taille_ADN=0
    counter=0
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        text="frag"+"{0:04d}".format(counter)   
        dict_new.update({str(text): str(record.seq)})
        sequences.append(str(record.seq))
        ids.append(text)
        Frag_tmp=Fragment(str(text), str(record.seq), 1)
        Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(), 0)
        """on remplie les lists des fragments"""
        Fragments.append(Frag_tmp)
        Fragments_Inverse.append(Frag_tmp_inverse)
        taille_ADN+=len(str(record.seq))
        counter+=1

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de gauche -> droite.
        L'orientation des fragments 'Right' est de gauche -> droite. """
    D_suff_pref=Chevauch_Orientation(Fragments, Fragments)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de gauche -> droite. 
        L'orientation des fragments 'Right' est de droite -> gauche."""
    D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de droite -> gauche. 
        L'orientation des fragments 'Right' est de gauche -> droite."""
    D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)

    """Les chevauchements avec: 
        L'orientation des fragments 'Left' est de droite -> gauche. 
        L'orientation des fragments 'Right' est de droite -> gauche."""
    D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)

    individu=[]
    Res_heuristique=Heuristique_Orient(D_suff_pref,D_suff_suff,\
            D_pref_pref,D_pref_suff,Fragments,Fragments_Inverse)
    print("Resultat de l'heuristique: ",len(Res_heuristique))
    Rest=[]
    Res_ids=[x.Id_Frag for x in Res_heuristique]
    for x in Fragments:
        if x.Id_Frag not in Res_ids:
            Rest.append(x)

    Res_heuristique.extend(Rest)

    seqs=[dict_new[id_fr.Id_Frag] for id_fr in Res_heuristique]

    #Resultat=Pals(seqs, [x.Id_Frag for x in Res_heuristique])
    Resultat=Pals(sequences,ids)
    for Id in Resultat:
    	frag_tmp=Fragment(Id, dict_new[Id], 1)
    	individu.append(frag_tmp)
    
    fit=fitnessNbContig_Orient(individu)
    
    #fit[5] contient les contigs resultants de pals
    print(fit[5])
print("\nTemps d execution : %.2f secondes --" % (time.time() - main_start_time))    
