#include <stdio.h>
#include <stdlib.h>
#include "PALS.h"
#include <string.h>

int main(){

	Fragment **solution;
	char tmp[11];
	char fragments[1000][1000];
	int fragmentCount = 0;
	int i;
	int nbr_contigs = 1;

	filename[] =
		 "dna-instances/x60189_4.dat";
		// "dna-instances/x60189_5.dat";
		// "dna-instances/x60189_6.dat";
		// "dna-instances/x60189_7.dat";
		// "dna-instances/m15421_5.dat";
		// "dna-instances/m15421_6.dat";
		// "dna-instances/m15421_7.dat";
		// "dna-instances/j02459_7.dat";
		// "dna-instances/38524243_4.dat";
		// "dna-instances/38524243_7.dat";
		

	FILE* fragmentFile = NULL;
	if(fragmentFile = fopen(filename,"r"))
	{
		while(!feof(fragmentFile))
		{
			if(!fscanf(fragmentFile,"%s\n",tmp))
				break;
			printf("%s->",tmp);
			if(!fscanf(fragmentFile,"%s\n",fragments[fragmentCount]))
				break;
			printf(" %d bases\n",strlen(fragments[fragmentCount]));
			fragmentCount++;
		}
        fclose(fragmentFile);		
	}
//----------------------------------------------------------------------------
	solution = (Fragment**) calloc(fragmentCount,sizeof(Fragment*));
	for(i = 0; i<fragmentCount;i++)
		solution[i] = createFragment(fragments[i]);

	PALS(solution,fragmentCount);


	/* Affichage de la solution
	printf("Solution finale :\n");
	for(i = 0;i <fragmentCount-1; i++)
		printf("%3d F%2d %3d\n",getOverlap(solution[i],LEFT),i, getOverlap(solution[i],RIGHT));
//*/


	system("PAUSE");
	return 0;
}