#ifndef PALS_H_INCLUDED
#define PALS_H_INCLUDED

	#define min(a,b) (a<=b?a:b)
	#define max(a,b) (a>=b?a:b)
	// Type représentant l'orientation d'un fragment
	typedef enum Orientation {
		LEFT = 0, 
     	RIGHT = 1
		} Orientation;

	typedef struct Info_Fragment{
		int id_frag;
		Orientation orientation;
		char* sequence;

	}Info_Fragment;

	// Structure de données représentant un fragment
	typedef struct Fragment{
		int length;
		Orientation orientation;
		char* sequence;
		int overlap[2];
	} Fragment;

	// Structure de donnée représentant un mouvement candidat
	typedef struct CandidateMovement{
		int i,
			j,
			iFitness,
			jFitness,
			fitnessVariation,
			contigVariation;
	} CandidateMovement;

	// Structure de données représentant un mouvement sélectionné
	typedef struct Movement{
		int i,
			j,
			iFitness, // 
			jFitness;
	} Movement;

	// Structure de données représentant un contig
	typedef struct Contig{
		int length;
		char* sequence;
		int* coverage;
	} Contig;
	
	Info_Fragment* create_info_Fragment(char sequence[], int id);
	Fragment* createFragment(char sequence[]); // Création d'un fragment à partir d'une chaîne de caractère <sequence>
	Fragment* copyFragment(Fragment* g, int reverseOrientation); // Copie d'un fragment <g> (avec éventuellement une invension d'orientation)
	void destroyFragment(Fragment* f); // Libération de la mémoire allouée dynamquement pour le fragment <f> [TO-DO : free(f->sequence]
	char* getSequence(Fragment* f); // Récupérer la séquence du fragment <f> pour un affichage
	
	#define getOverlap(f, direction) (f->overlap[f->orientation ^ direction]) 
	// Récupérer le chevauchement enregistré 
	//du préfixe <direction = LEFT> ou du suffixe 
	//<direction = RIGHT> du fragment <f>
	
	#define setOverlap(f, direction, newOverlapScore) (f->overlap[f->orientation ^ direction] = newOverlapScore) 
	// Modifier le chevauchement du préfixe <direction = LEFT> 
	//ou du suffixe <direction = RIGHT> 
	//du fragment <f> par la valeur <overlapScore>
	
	#define flipFragment(f,g) {f->orientation = (Orientation) (f->orientation ^ (Orientation) RIGHT);g->orientation =(Orientation) (g->orientation ^ (Orientation) RIGHT);}	
	// Inverser l'orientation du fragment <f>
	
	#define baseAt(a, b) (a->sequence[b]) 
	// Récupérer base azotée n°<index> du fragment <f>
	
	void swapFragments(Fragment* fragmentVector[], int i, int j); 
	// Echanger les positions <i> et <j> des fragments de 
	// la solution <fragmentVector>
	
	#define reverseObjective(candidateMovement) {candidateMovement.contigVariation = - candidateMovement.contigVariation; candidateMovement.fitnessVariation = - candidateMovement.fitnessVariation;}


	#define reverseOverlap()

	/*#ifndef WIN32
		char *strrev(char *str); 
		// Inversion d'une chaîne de caractères 
		//(pour les systèmes non-windows)[malloc]
	#endif*/
		
	void generateInitialSolution(Fragment* fragmentVector[], int length); // Génération de la solution initiale (aléatoire) [TO-DO : à réécrire]
	void calculateDelta(Fragment* fragmentVector[],int length, CandidateMovement* candidateMovement); // Calcul d'un mouvement candidat
	Movement* selectMovement(CandidateMovement candidateMovements[], int cmLength); // Sélection d'un mouvement parmi la liste des mouvements candidats <candidateMovements> [TO-DO : free(CandidateMovement)]
	void applyMovement(Fragment* fragmentVector[], int length, Movement* movement); // Application du mouvement <movement> à la solution <fragmentVector> free(Movement)
	void PALS(Fragment* fragmentVector[], int length, Info_Fragment* info_Vector[]); // Programme principal de la méthode PALS [TO-DO : Problème de convergence]
#endif // PALS_H_INCLUDED
	