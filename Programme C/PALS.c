#include "PALS.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>


int PALS_CUTOFF = 50;
int meilleur_contig=0;
//on initialise Frags avec l'ordre de l'individu pour qu'on le modifie a travers PALS
Info_Fragment **infos_solution;


/*#ifndef WIN32
	char* strrev(char* str)
	{
		char* reverse = NULL;
		int i, length;
		if(str) // Si la chaîne n'est pas nulle
		{
			length = strlen(str);
			reverse = (char*) malloc(length+1* sizeof(char));
			for(i = 0; i < length ; i++)
			{
				reverse[i] = str[length-i];
			}
			reverse[i] = '\0';
		}
		return reverse;
	}
#endif
*/

Info_Fragment* create_info_Fragment(char sequence[], int id){
    Info_Fragment* f = (Info_Fragment*) malloc(sizeof(Info_Fragment));
    f->id_frag=id;
    f->sequence = (char*) calloc(strlen(sequence)+1,sizeof(char)); // On ajoute +1 pour le caractère de fin de chaîne '\0'
    strcpy(f->sequence, sequence);
    f->orientation = LEFT;
    return f;
}
// Création d'un fragment à partir d'une chaîne de caractère <sequence>
Fragment* createFragment(char sequence[]){
    Fragment* f = (Fragment*) malloc(sizeof(Fragment));
    f->length = strlen(sequence);
    f->sequence = (char*) calloc(f->length+1,sizeof(char)); // On ajoute +1 pour le caractère de fin de chaîne '\0'
    strcpy(f->sequence, sequence);
    f->orientation = LEFT;
    f->overlap[LEFT] = f->overlap[RIGHT] = 0;
    return f;
}

// Copie d'un fragment <g> (avec éventuellement une invension d'orientation)
Fragment* copyFragment(Fragment* g, int reverseOrientation){
    Fragment* f = (Fragment*) malloc(sizeof(Fragment));
    f->length = g->length;
    f->sequence = (char*) calloc(f->length+1,sizeof(char));
    strcpy(f->sequence, g->sequence);
    f->orientation = !reverseOrientation ? g->orientation :(g->orientation ? LEFT : RIGHT);
    f->overlap[LEFT] = g->overlap[LEFT];
    f->overlap[RIGHT] = g->overlap[RIGHT];
    return f;
}

// Libération de la mémoire allouée dynamquement pour le fragment <f>
void destroyFragment(Fragment* f){
    free(f->sequence);
	f->sequence = NULL;
    free(f);
}

// Récupérer la séquence du fragment <f> pour un affichage
/*
char* getSequence(Fragment* f){
    char* sequence;
    sequence = (char*) calloc(f->length,sizeof(char));

	if(f->orientation == 0)
	{
		strcpy(sequence, f->sequence);
	}
	else
		sequence = strrev(f->sequence);
    return sequence;
}
*/

// Vérifier si le suffixe du fragment <f> et le préfixe du fragment <g>
// se chevauchent sur une longueur <overlapScore>
int overlaps(Fragment* f, Fragment* g, int overlapScore){
    int fLength = f->length, gLength = g->length, i;

	// Effectuer le calcul du chevauchement selon l'orientation de <f> et de <g>
    if(f->orientation == LEFT){
        if(g->orientation == LEFT)
			// Suffixe(f) x Prefixe(g)
            for(i = 0; i < overlapScore; i++)
            {
				if(baseAt(f,fLength-overlapScore+i) != baseAt(g,i))
                    return 0;
            }
        else
			// Suffixe(f) x Suffixe(g)
            for(i = 0; i < overlapScore; i++)
            {
                if(baseAt(f,fLength-overlapScore+i) != baseAt(g,gLength-1-i))
                    return 0;
            }
    }
	else{
        if(g->orientation == LEFT)
			// Préfixe(f) x Prefixe(g)
            for(i = 0; i < overlapScore; i++)
            {
				if(baseAt(f,overlapScore-1-i) != baseAt(g,i))
                    return 0;
            }
        else
			// Préfixe(f) x Suffixe(g)
            for(i = 0; i < overlapScore; i++)
            {
                if(baseAt(f,i) != baseAt(g,gLength-overlapScore+i))
                    return 0;
            }
    }
    return 1;
}

// Récupérer après calcul le chevauchement maximal entre le suffixe 
//de <f> et le préfixe de <g> en partant de <initialOverlap> (par défaut <initialOverlapScore = 0>)
int fitness(Fragment* f, Fragment* g, int initialOverlapScore){
    int fLength = f->length,
        gLength = g->length,
		maximumPossibleOverlap = min(fLength, gLength), // Meilleur chevauchement possible
		bestOverlapScore = 0, // Valeur du meilleur chevauchement entre <f> et <g> (initialement 0)
		i;


	// D'abord on cherche un chevauchement meilleur que initialOverlapScore
    for(i = initialOverlapScore; i < maximumPossibleOverlap; i++)
        if(overlaps(f,g,i))
			bestOverlapScore = i;
    if(bestOverlapScore == 0) // Si aucun chevauchement meilleur n'est trouvé
		// Chercher un chevauchement <= initialOverlapScore
		for(i = PALS_CUTOFF; i<initialOverlapScore; i++)
            if(overlaps(f,g,i))
                bestOverlapScore = i;
	return bestOverlapScore;
}

// Echanger les positions <i> et <j> des fragments de la solution <fragmentVector>
void swapFragments(Fragment* fragmentVector[], int i, int j){
	Fragment *tmp = fragmentVector[i];
	fragmentVector[i] = fragmentVector[j];
	fragmentVector[j] = tmp;

	Info_Fragment *tmp2 = infos_solution[i];
	infos_solution[i] = infos_solution[j];
	infos_solution[j] = tmp2;
}



// Génération de la solution initiale (aléatoire)
void generateInitialSolution(Fragment* fragmentVector[], int length){
	int i, j, k;
	srand(time(NULL));

	// Calcul des chevauchements initiaux
	for(i = 0; i < length-1; i++)
	{
		j = fitness(fragmentVector[i],fragmentVector[i+1],PALS_CUTOFF);
		setOverlap(fragmentVector[i],RIGHT,j);
		setOverlap(fragmentVector[i+1],LEFT,j);

	}

	// Permutations aléatoires des fragments de
	for(i = 0; i < length; i++)
	{

		j = i + rand() % (length-i);
		swapFragments(fragmentVector,i,j);

		k = rand() % 4;
		switch(k)
		{
			case 1: flipFragment(fragmentVector[i],infos_solution[i]);
					break;
			case 2: flipFragment(fragmentVector[j],infos_solution[j]);
					break;
			case 3: flipFragment(fragmentVector[i],infos_solution[i]);
					flipFragment(fragmentVector[j],infos_solution[j]);
					break;
			default:;
		}
		flipFragment(fragmentVector[i],infos_solution[i]);
		flipFragment(fragmentVector[j],infos_solution[j]);
	}
	//*/

/*	// Affichage de la solution
	printf("Solution initiale :\n");
	for(i = 0;i <length; i++)
		printf("Fragment %d => %s | L = %d | R = %d\n",i,getSequence(fragmentVector[i]), getOverlap(fragmentVector[i],LEFT),getOverlap(fragmentVector[i],RIGHT));
//*/

}

// Calcul d'un mouvement candidat (i <= j)
void calculateDelta(Fragment* fragmentVector[],int length, CandidateMovement* candidateMovement){

	int i = candidateMovement->i,
		j = candidateMovement->j,
		fitnessVar = 0,
		contigVar = 0,
		tmpFitness = 0;
	candidateMovement->iFitness = candidateMovement->jFitness = 0;

	// Supprimession du chevauchement F[i-1]F[i]
	tmpFitness  = getOverlap(fragmentVector[i],LEFT);

	if(tmpFitness >= PALS_CUTOFF)
		contigVar++;
	fitnessVar -= tmpFitness;

	// Ajout du chevauchement F[i-1]F[j]'
	if(i > 0)
	{
		tmpFitness = fitness(fragmentVector[i-1],fragmentVector[j],max(tmpFitness,PALS_CUTOFF));
		if(tmpFitness >= PALS_CUTOFF)
			contigVar--;
		candidateMovement->jFitness = tmpFitness;
		fitnessVar += tmpFitness;
	}

	// Supprimession du chevauchement F[j]F[j+1]
	tmpFitness = getOverlap(fragmentVector[j],RIGHT);
	if(tmpFitness >= PALS_CUTOFF)
		contigVar++;
	fitnessVar -= tmpFitness;
	// Ajout du chevauchement F[i]'F[j+1]
	if(j<length-1)
	{
		tmpFitness  = fitness(fragmentVector[i],fragmentVector[j+1],max(tmpFitness,PALS_CUTOFF));
		if(tmpFitness >= PALS_CUTOFF)
			contigVar--;
		candidateMovement->iFitness = tmpFitness;
		fitnessVar += tmpFitness;
	}

	// Mise à jours du mouvement candidat
	candidateMovement->fitnessVariation = fitnessVar;
	candidateMovement->contigVariation = contigVar;
}

// Sélection d'un mouvement parmi la liste des mouvements candidats <candidateMovements>
Movement* selectMovement(CandidateMovement candidateMovements[], int cmLength){

	int index, // Variable de boucle pour parcourir les mouvements candidats
		best = 0; // indice du meilleur mouvement (initialement 0)
	int direction = rand() % 2;
	int  init = 0, stop = cmLength, increment = 1;
	Movement* selectedMovement = NULL; // Mouvement à appliquer (initialement NULL)
	if(direction)
	{
		best = cmLength-1;
		init = cmLength-2;
		stop = -1;
		increment = -1;
	}

	for(index = init; index != stop; index += increment)
	//for(index = 1; index < cmLength; index++)
		if((candidateMovements[index].contigVariation < 
			candidateMovements[best].contigVariation) || ((candidateMovements[index].contigVariation == candidateMovements[best].contigVariation) && (candidateMovements[index].fitnessVariation > candidateMovements[best].fitnessVariation)))
		{
			best = index;

		}

		if((candidateMovements[best].contigVariation < 0) ||  ((candidateMovements[best].contigVariation == 0) && (candidateMovements[best].fitnessVariation > 0)))
		{
			selectedMovement = (Movement*) malloc(sizeof(Movement));
			selectedMovement->i = candidateMovements[best].i;
			selectedMovement->j = candidateMovements[best].j;
			selectedMovement->iFitness = candidateMovements[best].iFitness;
			selectedMovement->jFitness = candidateMovements[best].jFitness;
		}

	return selectedMovement;
}

// Application du mouvement <movement> à la solution <fragmentVector>
void applyMovement(Fragment* fragmentVector[], int length, Movement* movement){
	int i = movement->i,
		j = movement->j;

	// Mise à jour du chevauchement F[i]'F[j+1]
	setOverlap(fragmentVector[i],LEFT,movement->iFitness);
	if(j<length-1)
		setOverlap(fragmentVector[j+1], LEFT, movement->iFitness);

	// Mise à jour du chevauchement F[i-1]F[j]'
	setOverlap(fragmentVector[j], RIGHT, movement->jFitness);
	if(i>0)
		setOverlap(fragmentVector[i-1], RIGHT, movement->jFitness);

	// Inversion de la sous-permutation des fragments de la solution compris entre <i> et <j> inclus
	while(i<j)
	{
		swapFragments(fragmentVector,i,j);
		flipFragment(fragmentVector[i],infos_solution[i]);
		flipFragment(fragmentVector[j],infos_solution[j]);
		i++; j--;
	}

	// Une dernière inversion si le nombre de fragments est impair
	if(i == j)
	{
		flipFragment(fragmentVector[i],infos_solution[i]);
	}

	// Libération de la mémoire allouée pour <movement>
	//free(movement);
	//movement = NULL;
}

void PALS(Fragment* fragmentVector[], int length, Info_Fragment* info_Vector[]){
	int i, j, k,temp,
        mIndex = 0, // indice du mouvement candidat actuel
		solutionImproved  = 1; // Spécifie si la solution est améliorée
	int movementsPerIteration = (length+1)*length/2-1 ; // Nombre de mouvements candidats à calculer par itération
	int iterations = 0; // Nombre d'itérations
	CandidateMovement* candidateMovements = NULL; // Liste des mouvements candidats
	Movement* selectedMovement = NULL;
	int nbr_contigs = 1;

	generateInitialSolution(fragmentVector,length); // Génération de la solution initiale

	candidateMovements = (CandidateMovement*) calloc(movementsPerIteration,sizeof(CandidateMovement));

	for(i = 1; i<length; i++)
		if(!getOverlap(fragmentVector[i],LEFT))
			nbr_contigs++;

// Première itération ---------------------------------------------------------------
		// Nouvelle itération
	iterations++;
	// Calcul des mouvements candidats (sans mouvements équivalents [i <= j])
		for(j = 0; j < length-1; j++) // j : longueur de la sous-permutation - 1 [length - 1] car length est la longueur de la séquence
			for(i = 0; i+j < length; i++) // i : indice de début de la sous permutation
			{
			candidateMovements[mIndex].i = i;
			candidateMovements[mIndex].j = i+j;
			calculateDelta(fragmentVector,length,&candidateMovements[mIndex]);
			mIndex++;
			}
	// Selection d'un mouvement dans la liste candidateMovements
	selectedMovement = selectMovement(candidateMovements,movementsPerIteration);

	if(selectedMovement == NULL) // Si aucun mouvement n'est choisi
	{
		solutionImproved = 0; // Plus aucune amélioration de la solution
		printf("\nAucun mouvement appliqué");
	}
	else
	{
		//printf("\nMouvement appliqu\x82 : <%d-%d>", selectedMovement->i,selectedMovement->j);
		applyMovement(fragmentVector,length,selectedMovement);
		/*temp=Frags[selectedMovement->i];
		Frags[selectedMovement->i]=Frags[selectedMovement->j];
		Frags[selectedMovement->j]=temp;

		Orient[selectedMovement->i]=fragmentVector[selectedMovement->j]->orientation;
		Orient[selectedMovement->j]=fragmentVector[selectedMovement->i]->orientation;
		*/}

//	printf("\nfin de l'iteration %d\n",iterations);


// Boucle principale -----------------------------------------------------------------
	while(solutionImproved)
	{
		// Nouvelle itération
		iterations++;
		mIndex = 0;
		k = 0;
		// Calcul des mouvements candidats (sans mouvements équivalents [i <= j])
		for(j = 0; j < length-1; j++)
			for(i = 0; i+j < length; i++)
			{
				if((i == selectedMovement->i) && (j == selectedMovement->j))
					reverseObjective(candidateMovements[mIndex])
				else if((i > selectedMovement->i) && (j < selectedMovement->j))
					{ calculateDelta(fragmentVector,length,&candidateMovements[mIndex]);}
					//reverseOverlap();
				else if((i < selectedMovement->i) && (j > selectedMovement->j));
				else{ 
					calculateDelta(fragmentVector,length,&candidateMovements[mIndex]); 
					k++;}

				mIndex++;
			}

		// Selection d'un mouvement dans la liste candidateMovements
		selectedMovement = selectMovement(candidateMovements,movementsPerIteration);

		if(selectedMovement == NULL) // Si aucun mouvement n'est choisi
		{
			solutionImproved = 0; // Plus aucune amélioration de la solution
			free(selectedMovement);
			//printf("\nAucun mouvement appliqué");
		}
		else
		{
			//printf("\nMouvement applique : <%d-%d>", selectedMovement->i,selectedMovement->j);
			applyMovement(fragmentVector,length,selectedMovement);
			
			/*
			temp=Frags[selectedMovement->i];
			Frags[selectedMovement->i]=Frags[selectedMovement->j];
			Frags[selectedMovement->j]=temp;

			Orient[selectedMovement->i]=fragmentVector[selectedMovement->j]->orientation;
			Orient[selectedMovement->j]=fragmentVector[selectedMovement->i]->orientation;
			*/
			free(selectedMovement);
		}
		//printf("\nfin de l'iteration %d\n",iterations);
	}
	while(solutionImproved); // Tant qu'il y a une amélioration de la solution


	free(candidateMovements);

	// printf("\nNombre d'it\x82rations : %d\n",iterations);
	int tmp_cont=0;

	//printf("\nNombre total de contigs de la solution initiale: %d", nbr_contigs);
	for(i = 0, nbr_contigs = 0; i<length; i++)
			if(!getOverlap(fragmentVector[i],LEFT)){
				//printf("\norientation %d, %d est: %d", i,infos_solution[i]->id_frag, fragmentVector[i]->orientation);
				nbr_contigs++;
				tmp_cont=0;
				//contigs[nbr_contigs]=0;				

			}
			else{
				//contigs[nbr_contigs][tmp_cont]=infos_solution[i]->id_frag;
				tmp_cont++;
				if (meilleur_contig<=tmp_cont){
					meilleur_contig=tmp_cont;
					//l=nbr_contigs;
				}
				//printf("contig: %d, frag:%s", nbr_contigs,fragmentVector[i]->sequence);

			}
	
	printf("\nNombre total de contigs de la solution finale: %d", nbr_contigs);
	printf("\nTaille de Meilleur Contig: %d\n", meilleur_contig);

	char str[3],str2[3],str3[3];
	/*FILE *fp = fopen("Resultats/6.Stats_PALS.csv","a+");

   	fputs("\nMeilleur individu:, ",fp);
   	sprintf(str, "%d,", nbr_contigs);
   	fputs(str,fp);
    sprintf(str2, "%d,", meilleur_contig);
    fputs(str2,fp);
    fclose(fp);

	*/

    /*FILE *fp = fopen("Resultats/test.dat","a+");
	for(i = 0;i <length; i++){
		//printf("Fragment %d => %s \n\n",i,getSequence(fragmentVector[i]));
		if (fp != NULL){
			
				fputs(">frag",fp);
				sprintf(str, "%d", infos_solution[i]->id_frag);
				fputs(str,fp);
				fputs("\n",fp);
        		fputs(fragmentVector[i]->sequence,fp);
        		fputs("\n",fp);
    	}	
	}*/
		

}
/* Create a function that can free our pointers */
void freeptr(void *ptr)
{
    free(ptr);
}

int * main(int taille_individu,char* individual[1000], int ordre_frags[1000]){

	Fragment **solution;
	char tmp[11];
	char fragments[1000][1000];
	int fragmentCount = 0;
	int i;
	int nbr_contigs = 1;
	int *Frags;

	Frags = (int*) malloc(taille_individu * sizeof(int));


	char filename[] =
		 "dna-instances/x60189_4.dat";
		// "dna-instances/x60189_5.dat";
		// "dna-instances/x60189_6.dat";
		// "dna-instances/x60189_7.dat";
		// "dna-instances/m15421_5.dat";
		// "dna-instances/m15421_6.dat";
		// "dna-instances/m15421_7.dat";
		// "dna-instances/j02459_7.dat";
		// "dna-instances/38524243_4.dat";
		// "dna-instances/38524243_7.dat";
		

	/*FILE* fragmentFile = NULL;
	if(fragmentFile = fopen(filename,"r"))
	{
		while(!feof(fragmentFile))
		{
			if(!fscanf(fragmentFile,"%s\n",tmp))
				break;
			//printf("%s->",tmp);
			if(!fscanf(fragmentFile,"%s\n",fragments[fragmentCount]))
				break;
			//printf(" %ld bases\n",strlen(fragments[fragmentCount]));
			fragmentCount++;
		}
        fclose(fragmentFile);		
	}*/
//----------------------------------------------------------------------------
	solution = (Fragment**) calloc(taille_individu+1,sizeof(Fragment*));
	infos_solution=(Info_Fragment**) calloc(taille_individu+1,sizeof(Info_Fragment*));
	
	for(i = 0; i<taille_individu;i++)
		solution[i] = createFragment(individual[i]);

		
	for (int i = 0; i < taille_individu; ++i)
	{
		infos_solution[i]=create_info_Fragment(individual[i],i);
	}
	


	PALS(solution,taille_individu,infos_solution);


	for(int j=0; j<taille_individu;j++){
		Frags[j]=infos_solution[j]->id_frag;
	}
	/*
	for (int i = 0; i < taille_individu; ++i)
	{
		printf("%d, %d\n", infos_solution[i]->id_frag, infos_solution[i]->orientation);
	}*/

	//affichage de resultats de l'ordre aprés l'execution
	/*char* tempp;
	for(i = 0; i<taille_individu;i++){
		if ((int)Orient[i]==0)
			tempp="Gauche";
		else
			tempp="Droite";
		printf("frag%d : %s\n",Frags[i],tempp);
 	}*/

	 /*Affichage de la solution
	printf("Solution finale :\n");
	for(i = 0;i <fragmentCount-1; i++)
		printf("%3d F%2d %3d\n",getOverlap(solution[i],LEFT),i, getOverlap(solution[i],RIGHT));
//*/
	for (int i=0; i<taille_individu; i++) {

	free(infos_solution[i]->sequence);
	infos_solution[i]->sequence = NULL;
    free(infos_solution[i]);

    free(solution[i]->sequence);
	solution[i]->sequence = NULL;
    free(solution[i]);
	}

    sleep(0.5);              
	//system("read -p 'Appuyez sur Entrer pour Continuer...' var");
	return Frags;

}


