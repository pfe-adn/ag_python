import random, copy
from dataclasses import dataclass

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse


def efface_repetition_Orient(left, right):
    """ Effacer la partie repétée de 2 fragments qui se chevauchent"""
    for i in range(len(left)):
        if left[i:] == right[:len(left)-i]: 
            #on supprime toute la partie de left repeté dans right
            right=''.join(right.split(right[:len(left)-i]))
            return right
    return right


def genere_seq_Orient(contig):
    """Prends un contig et genere sa séquence equivalente"""
    sequence=''      
    for i in range(len(contig)):
        #sequence+= efface_repetition_Orient(contig[i].Seq_Frag,\
        #                                contig[i+1].Seq_Frag)
        if contig[i].Orientation==1:
            sequence+=contig[i].Seq_Frag
        else:
            tmp=copy.deepcopy(contig[i])
            sequence+=tmp.Inverse_Sequence()
    return sequence

def genere_seq_de_contig_Orient(contigs):
    """Genere la sequence equivalante d'une liste des contigs"""
    l=[]
    for contig in contigs:
        l.append(genere_seq_Orient(contig))
    return l

def meilleur_contig_Orient(contigs):
    """determine le meilleur contig a partir d'une liste des contigs"""
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    """on determine les contigs qui ont le plus grand nombre de fragments
    (car on peut avoir plusieurs contigs qui ont le même nombre de fragments)."""
    meilleurs_contigs=[f for f in contigs if len(f)==tailleBestContig]

    """après on genere les sequences pour ces contigs."""
    k=genere_seq_de_contig_Orient(meilleurs_contigs)
        
    """après on determine le meilleur contig en comparant la longueur 
    des sequences de ces contigs."""
    meilleur_contig= meilleurs_contigs[k.index(max(k,key=lambda x: len(x)))]
    chevauchement=max(k,key=lambda x: len(x))

    return meilleur_contig,chevauchement

def Rempli_Aleatoire(data):
    res=[]
    temp=data[:]
    for y in range(len(temp)):
        val=random.choice(temp)
        temp.remove(val)
        val.Orientation=random.randint(0, 1)
        if val.Orientation==0:
            val.Seq_Frag=val.Inverse_Sequence()
        res.append(val)
    return res
