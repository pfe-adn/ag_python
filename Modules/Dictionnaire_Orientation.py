#coding:utf-8
"""
Module Dictionnaire
"""
from dataclasses import dataclass
import re
@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    #Orientation est de gauche a droite si 1, et de droite a gauche si 0 
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse

#retourner la plus grande chaine qui chevauche entre les sequences left et right 
def ChevechOLCGRE(left, right):
    indices=[m.start() for m in re.finditer(left[-10:],right)]
    for indice in indices[::-1]:
        if len(left)<indice+10:
            indices.remove(indice)
        else:
            if left[-indice-10:]== right[:indice+10]:
                return(left[-indice-10:])
    return ''


def ChevechTousOLCGRE(reads):
    d = dict()
    for name1, seq1 in reads.items():
        for name2, seq2 in reads.items():
            if name1 == name2: #sauter la diagnale  
                continue
            if name1 not in d:
                d[name1] = dict()#un dictionnaire contient une clé et sa valeur [<clé>:<valeur>]
            d[name1][name2] = len(ChevechOLCGRE(seq1, seq2))

    return d

def Chevauch_Orientation(Fragments_1, Fragments_2):
    """Retourne un dictionnaire qui contient les chevauchements entre toutes les 
    fragments des 2 listes en input"""
    d=dict()
    Name1=Fragments_1[:]
    Name2=Fragments_2[:]
    try:
    
        for name1 in Fragments_1:
            for name2 in Fragments_2:
                if name1.Id_Frag==name2.Id_Frag:
                    continue 
                else:
                    if name1.Id_Frag not in d:
                        d[name1.Id_Frag]=dict()
                    d[name1.Id_Frag][name2.Id_Frag]=\
                    len(ChevechOLCGRE(name1.Seq_Frag,name2.Seq_Frag))
        return d
    
    except TypeError:
        print ("Les inputs doivent etre deux listes des instances de classe Fragment")

    """
    for i in range(len(Name1)):
        for j in range(len(Name1)):
            if Name1[i].Id_Frag==Name2[j].Id_Frag:
                continue
            if Name1[i].Id_Frag not in d:
                d[Name1[i].Id_Frag]=dict()
            d[Name1[i].Id_Frag][Name2[j].Id_Frag]=\
            len(ChevechOLCGRE(Name1[i].Seq_Frag, Name2[j].Seq_Frag))
    return d
    """

