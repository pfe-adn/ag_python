
def ValeurMax(d1,d4):
    m1 = max (d1.values())
    m4 = max (d4.values())
    m5 = max(m1,m4)
    if m5==m1:
        for k in d1:
            if d1[k] == m5:
                return k,1

    if m5==m4:
        for k in d4:
            if d4[k] == m5:
                return k,1

def ValeurMax_mutation(left,d1,d2,d3,d4):
    m1 = max (d1.values())
    m2 = max (d2.values())
    m3 = max (d3.values())
    m4 = max (d4.values())
    
    if left.Orientation==1:
        m5 = max(m1,m2)
        if m5==m1:
            for k in d1:
                if d1[k] == m5:
                    return k,1

        if m5==m2:
            for k in d2:
                if d2[k] == m5:
                    return k,0

    if left.Orientation==0:
        m5 = max(m3,m4)
        if m5==m3:
            for k in d3:
                if d3[k] == m5:
                    return k,1

        if m5==m4:
            for k in d4:
                if d4[k] == m5:
                    return k,0

def TrouveOrdre_mutation_test(first,d1,d2,d3,d4,Individual, Fragments_Inverse):
    f=first
    Id=first.Id_Frag
    orient=first.Orientation
    ordre=[first]
    i=0
    """si l'orientation de premier element est 1, on cherche seulement dans
    les matrices dont l'orientation du fragment left est de gauche->droite
    """
    if orient==1:
        max_chev=max(max(d1[Id].values()), max(d2[Id].values()))
    """Sinon les 2 autres matrices"""
    if orient==0:
        max_chev=max(max(d3[Id].values()), max(d4[Id].values()))


    tmp=ValeurMax_mutation(first,d1[Id],d2[Id],d3[Id],d4[Id])

    while((max_chev >= 10) and (tmp[0] != Id)):
        Id=tmp[0]
        Orient=tmp[1]
        tmp_frag=Fragment(Id, '',Orient)
        nextRead=Create_Sequence(tmp_frag, Individual, Fragments_Inverse)
        ordre.append(nextRead)
        #peut etre ajouter des conditions pour l'orientation    
        if orient==1:
            max_chev=max(max(d1[Id].values()), max(d2[Id].values()))
        if orient==0:
            max_chev=max(max(d3[Id].values()), max(d4[Id].values()))
        tmp=ValeurMax_mutation(first,d1[Id],d2[Id],d3[Id],d4[Id])

    return ordre

def Create_Sequence(Fragment, Fragments, Fragments_Inverse):
    if Fragment.Orientation==1:
        for element in Fragments:
            if element.Id_Frag==Fragment.Id_Frag:
                Fragment.Seq_Frag=element.Seq_Frag[:]
    else:
        for element in Fragments_Inverse:
            if element.Id_Frag==Fragment.Id_Frag:
                Fragment.Seq_Frag=element.Seq_Frag[:]
    return Fragment
