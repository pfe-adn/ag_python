from ctypes import *
import ctypes, time, glob, os,sys
sys.path.append('./Modules')
from Bio import SeqIO
from Fonctions_Meta_Orient import *
from dataclasses import dataclass


def Pals(individual,Ids,decalage):
    so_file="../Programme C/PALS.so"
    PALS=CDLL(so_file)

    #Nombre de fragments
    length=len(individual)

    # main retourne un pointeur integers de taille la même que la taille de fichier
    PALS.main.restype=ctypes.POINTER(ctypes.c_int * length)

    #seqs contient les sequences aprés l'ncodage UTF-8
    a=[]
    for element in individual:
        a.append(element.encode('utf-8'))

    seqs = (ctypes.c_char_p * length)() 
    seqs[:]=a

    #ids contient l'ordre des fragments de l'individuel
    ordre=[] 
    for element in Ids:
        ordre.append(int(element[4:]))
    ids = (ctypes.c_int * length)(*ordre)
    
	#sauvegarde le resultat dans fichier Res  (voir PALS.c)
    Res = PALS.main(length, seqs, ids)
    
    #sauvegarde sous forme d'une liste d'ordre pour utiliser dans metaheuristique
    Result = ["frag"+'{0:04d}'.format(x+decalage) for x in Res.contents]
    return Result

if __name__ == '__main__':

    filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
    #fileslim=filelist[0]
    for fileslim in filelist[0:1]:
        main_start_time = time.time()
        filename= os.path.split(fileslim)[1]
        dict_new={}
        sequences=[]
        ids=[]
        taille_ADN=0
        for record in SeqIO.parse(fileslim, "fasta"):
            index = record.id.split('.')
            dict_new.update({(record.id): str(record.seq)})
            sequences.append(str(record.seq))
            taille_ADN+=len(str(record.seq))
            ids.append(record.id)
    
        """f = open("Resultats/6.Stats_PALS.csv","a+")
        f.write("\t\tFichier: %s\t\t\t\n"% filename)
        f.write("\n\tnombre de contigs, taille de meilleur contig")
        f.write("\nInformations Initiales: ,%s"% str(len(ids))+","+ str(1))
        f.close()
        """
        res=Pals(sequences, ids)
        #print(res)
        temps_exec=time.time() - main_start_time 
        """
        f = open("Resultats/6.Stats_PALS.csv","a+")
        f.write("\nTemps d'execution: %.2f sec" % temps_exec)
        f.write("\n\n\n\n")
        f.close()
        time.sleep(3)"""


    print("\nTemps d execution : %.2f secondes --" % (time.time() - main_start_time))    

