import copy, time, glob, sys
sys.setrecursionlimit(10000)
from tkinter import *
from random import randrange
from Bio import SeqIO
import statistics

start_time = time.time()

def chevauchement(left, right):
    """ Fonction pour chercher si il existe un chevauchement entre deux fragments"""
    for i in range(len(left)):
        if left[i:] == right[:len(left)-i]: #tout les caracteres a partir du debut sauf les derniers len(left)-i
            return left[i:]#retourner la chaine du i à la fin
    return '' #si pas de chevauchement retourner chaine vide 

def ChevauchTous(reads):
    """ creation du dictionnaire des chevauchements"""
    d = dict()
    for name1, seq1 in reads.items():
        for name2, seq2 in reads.items():
            if name1 == name2: #sauter la diagnale  
                continue
            if name1 not in d:
                d[name1] = dict()#un dictionnaire contient une clé et sa valeur [<clé>:<valeur>]
            d[name1][name2] = len(chevauchement(seq1, seq2))
    return d

def TrouveFrag(d):  
        """ Trouver les fragments avec un chevauchement > seuil""" 
        l=list()  
        for i in d:
            BonfChevauchments = False
            for j in d[i]:
                if d[i][j] > 10:#10 est le seuil de chevauchement 
                    BonfChevauchments = True
            if  BonfChevauchments:
              l.append(i)
        return l
        
def ValeurMax(d):
    m = max (d.values())
    for k in d:
        if d[k] == m:
            return k


def TrouveOrdre(first, d):
    f=first
    if max(d[first].values()) < 10:#condition d'arret de l'appel recursif 
        return [first]
    else:
        nextRead = ValeurMax(d[first])
        if nextRead!=f:#pour eviter le loop des frags
            return [first] + TrouveOrdre(nextRead, d)
        else:
            return [first]#si il n'exsite pas de chauv

def Assmblage(LireOrdre, reads, Chevauchments):
    """ Assemblage des ordres obtenue"""
    genamee = ''
    for readname in LireOrdre[:-1]:#equivalent a fin de boucle en n-1 
        rightOverlap = max(x for x in Chevauchments[readname].values() if x >= 10)
        genamee += reads[readname][:-rightOverlap] #on part du debut jusqua -rightoverlap 
    genamee += reads[LireOrdre[-1]]#chercher le dernier element de la liste ou tableau lireOrdre 
    return genamee


def FindMax(lst): 
    maxList = max((x) for x in lst) 
    maxLength = max(len(x) for x in lst ) 
    return maxList, maxLength

def Heuristique(dict_chev, dict_meta):
    premierEl=TrouveFrag(dict_chev)
    ListFrag=[]
    List_ordre=[] 
    for i in premierEl: 
        order=TrouveOrdre(i,dict_chev)
        List_ordre.append(order)
        #consensus=Assmblage(order,dict_meta, dict_chev)
        #tailleDeConsensus=len(consensus)
        #ListFrag.append(consensus)
    meill_ordre=sorted(List_ordre,key=lambda x: len(x))[-1]
    return meill_ordre


if __name__ == '__main__':
    filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
    fileslim=filelist[1]
    dict_new = {}
    ids=[]
    sequences=[]
    records = list(SeqIO.parse(fileslim, "fasta"))
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        dict_new.update({(record.id): str(record.seq)})
        sequences.append(str(record.seq))
        ids.append(record.id)
    
    d = ChevauchTous(dict_new)

    a=Heuristique(d,dict_new)
    print("la taille de meilleur contig: ",len(a))
    print("\nTemps d execution Heuristique: %.2f secondes --" % (time.time() - start_time))    


"""meill_consens=sorted(ListFrag,key=lambda x: len(x))[-1]


print("Meilleur ordre(contig) obtenue est: ",meill_ordre,
    "\n\nTaile de meilleur chevauchment: ",len(meill_consens))
#print("****Le meilleur consensus est: ****",FindMax(ListFrag))
taille_meill_consens=len(meill_consens)
taille_meill_ordre=len(meill_ordre)


f = open("statstest.csv","a+")
f.write("Resultat heuristique: \t\t\t")
f.write("\nMeilleur chevauchment chevauchments, taille de l'ordre")
f.write("\n%d,%d\n\n" % (taille_meill_consens,taille_meill_ordre) )
f.close()
"""



""" géner un fichier excel en couleur du meilleur consensus obtenu
from xlsxwriter.workbook import Workbook
from datetime import datetime
dtt=datetime.now().strftime("%d-%m-%y-%Hh%M-%S")
fch="ficher"
ext=".xlsx"
filexcel=fch+dtt+ext
workbook = Workbook(filexcel)
worksheet = workbook.add_worksheet()

red = workbook.add_format({'color': 'red'})
blue = workbook.add_format({'color': 'blue'})
orange = workbook.add_format({'color': 'orange'})
green = workbook.add_format({'color': 'green'})

final=consensus.upper()
sequences = [final] #transformer la chaine final qui contient plusieurs sequences (une seule chaine de caract) en tableau de sequences en exploitant le separateur 

worksheet.set_column('A:A', 40)

for row_num, sequence in enumerate(sequences):
    format_pairs = []
    # Obtenir chaque caractère de base ADN de la séquence.
    for base in sequence.upper():
        if base == 'A' :
            format_pairs.extend((red, base))
        elif base == 'G' :
            format_pairs.extend((orange, base))
        elif base == 'T' :
            format_pairs.extend((green, base))
        elif base == 'C' :
            format_pairs.extend((blue, base))  
            
        else:
           
            format_pairs.append(base)

    worksheet.write_rich_string(row_num, 0, *format_pairs)

workbook.close()
"""