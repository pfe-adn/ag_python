#coding:utf-8
"""
Module Dictionnaire
"""
import copy,re
def ChevechOLCGRE(left,right):
    indices=[m.start() for m in re.finditer(left[-10:],right)]
    for indice in indices[::-1]:
        if len(left)<indice+10:
            indices.remove(indice)
        else:
            if left[-indice-10:]== right[:indice+10]:
                return(left[-indice-10:])
    return ''

#retourner la plus grande chaine qui chevauche entre les sequences left et right 
def ChevechOLCGRE_ancien(left, right):
    for i in range(len(left)):
        if left[i:] == right[:len(left)-i]: #tout les caracteres a partir du debut sauf les derniers len(left)-i
            return left[i:]  #retourner la chaine du i à la fin 
    return '' #si pas de chevauchement retourner chaine vide 


def ChevechTousOLCGRE(reads):
    d = dict()
    for name1, seq1 in reads.items():
        for name2, seq2 in reads.items():
            if name1!=name2:
                if name1 not in d:
                    d[name1]=dict()
                d[name1][name2]=len(ChevechOLCGRE(seq1,seq2))

    return d

def Chevauch_Orientation(Fragments_1, Fragments_2):
    """Retourne un dictionnaire qui contient les chevauchements entre toutes les 
    fragments des 2 listes en input"""
    d=dict()
    
    for name1, seq1 in Fragments_1.items():
        for name2,seq2 in Fragments_2.items():
            if name1!=name2:
                if name1 not in d:
                    d[name1]=dict()
                d[name1][name2]=len(ChevechOLCGRE(seq1,seq2))
    return d
