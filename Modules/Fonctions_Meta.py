import random
from dataclasses import dataclass

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    overlap: dict
    #Orientation est de gauche a droite si 1, et de droite a gauche si 0 
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse


def swap(a, b):
    """ Fonction de permutation"""
    return b, a

def Rempli_Aleatoire(data):
    res=[]
    temp=data[:]
    for i in range(len(temp)):
        val=random.choice(temp)
        res.append(val)
        temp.remove(val)
    return res

def Gen_list_Seq(ordre,dict_individu_seq):
    """Genere une liste des sequences des fragments avec l'ordre (ordre) en utilisant 
    le dictionnaire qui contient les sequences"""
    res=[]
    for element in ordre:
        res.append(dict_individu_seq[element])
    return res

def efface_repetition(left, right):
    """ Effacer la partie repétée de 2 fragments qui se chevauchent"""
    for i in range(len(left)):
        #tout les caracteres a partir du debut sauf les derniers len(left)-i
        if left[i:] == right[:len(left)-i]: 
            #on supprime toute la partie de left repeté dans right
            right=''.join(right.split(right[:len(left)-i]))
            return right
    return right

def genere_seq(contig,dict_individu_seq):
    """Prends un contig et genere sa séquence equivalente"""
    sequence=dict_individu_seq[contig[0]]       
    for i in range(len(contig)-1):
        sequence+= efface_repetition(dict_individu_seq[contig[i]], dict_individu_seq[contig[i+1]])
    return sequence

def genere_seq_de_contig(contigs,dict_individu_seq):
    """Genere la sequence equivalante d'une liste des contigs"""
    l=[]
    for contig in contigs:
        l.append(genere_seq(contig,dict_individu_seq))
    return l


def meilleur_contig(contigs, dict_individu_seq):
    """determine le meilleur contig a partir d'une liste des contigs"""
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #on determine les contigs qui ont le plus grand nombre de fragments
    #(car on peut avoir plusieurs contigs qui ont le même nombre de fragments).
    meilleurs_contigs=[f for f in contigs if len(f)==tailleBestContig]

    #après on genere les sequences pour ces contigs.
    k=genere_seq_de_contig(meilleurs_contigs,dict_individu_seq)
        
    #après on determine le meilleur contig en comparant la longueur des sequences de ces contigs.
    meilleur_contig= meilleurs_contigs[k.index(max(k,key=lambda x: len(x)))]  

    return meilleur_contig


def infos_population(population):
    """ Retourne: 
    1. une liste de nombre de chevauchements de chaque individu 
    2. une liste des individus (en fragments) correspendants   """
    info=[]
    f_nbr_chevauch=[]
    f_individus=[]
    for f in population:    
        f_nbr_chevauch.append(f[0])
        f_individus.append(f[1])
        info.append((f[0],f[1]))
    return info

def repetition_individus(population, affich, critere="individus"):
    """Retourne: 
    une liste qui contient les sommes de chevauchments et leur nombre de répétitions et le pourcentage
    dans la population
    """
    repitition=[]
    percentage=[]
    #la population contient une liste des tuples (nbr chev, individu) donc on les sépare
    list_chev=[x[0] for x in population]
    list_individus=[x[1] for x in population]
    #si le critère de calcule de répetition est par le nbr de chevauchements 
    #on parcoure la liste des nbr de chevauchment
    if critere=="chev":
        j=0
        list_a_parcourir=list_chev
    else:   
        j=1
        list_a_parcourir=list_individus
    
    i=0
    repitition_tmp=[]
    for individu in population:
        if individu[j] not in repitition_tmp:
            repitition_tmp.append(individu[j])

            #on sauvegarde le pourcentage dans un variable 
            percentage_individual= list_a_parcourir.count(individu[j])*100/len(list_a_parcourir)
            #on sauvegarde la repitition dans un variable
            repit_individu=list_a_parcourir.count(individu[j])
            #on garde seulement les individus qui se répétent plus de 10 fois
            if repit_individu>10:
                #on affiche l'individu, son nbr de répétition, et le pourcentage.
                repitition.append([individu[0],repit_individu, "%.2f%%" % (percentage_individual)])

    if affich:
        print("\n", repitition)
    return repitition

