	Ce fichier va contenir toutes les étapes que nous avons suivies lors 
de notre optimisation et amélioration de cette métaheuristique:

	chacune des 3 équipes ayant précédemment travaillé sur ce projet a utilisé 
différentes méthodes pour résoudre le problème d'assemblage. On vise a créer 
une metaheuristique hybride qui utilise ces 3 methodes et leurs résultats pour
trouver une solution plus précise.
	
Les solutions qu'on va mentionner si-dessous seront expliqués plus profondement dans notre mémoire avec les statistiques des resultats:
	
	1. Creation des individus hybride (en utilisant l'heuristique) sans prendre l'orientation des fragments en consideration: 

		0) population_size=800,
                                   generations= [50,100],
                                   crossover_probability=0.5 (et 1),
                                   mutation_probability=0.5 (et 1),
                                   elitism=True,
                                   maximise_fitness=True

		a) avant la création des individus, on exécute l'heuristique (créée par une des équipes 
	précédents) qui trouve le plus grand contig possible (recherche locale)

		b) on rempli la première partie de l'individu avec le contig recupéré de l'heuristique
	aprés ce qui reste on le rempli aléatoirement pour chaque individu.

		les resultats sont presentés dans des fichiers csv. 
			- le temps d'éxecution est petit et les resultats mieux qu'avec la creation aléatoire des individus.

	2. Creation des individus hybride (en utilisant l'heuristique) AVEC prendre l'orientation des fragments en consideration: 
		0) population_size=800,
                                   generations= [50,100],
                                   crossover_probability=0.5 (et 1),
                                   mutation_probability=0.5 (et 1),
                                   elitism=True,
                                   maximise_fitness=True

        a) le même principe que la precedente mais la structure des fragments est changé en un dataclass contenant l'id de fragment, la sequence, et l'orientation.

        - Le temps d'éxecution est plus long par rapport a la methode precedente surtt avec les plus grands fichiers, la difference est evidente ( par exemple le même fichier 125 seconds -> 520 seconds)


    3. le meme que 2. mais la mutation est hybride aussi:	
    	
    	a) on execute l'heuristique sur l'individu puis on place le plus grand contig au debut de nouveau individu, et on ajoute le reste aleatoirement.

    	- Le temps d'exécution est un peu plus long mais les resultats ne sont pas améliorés

    4. Un tout nouveau concept qu'on pense va etre meilleur que les solutions précedents:

    	0. on a remarqué que pals.c est trés rapide quand le nombre de fragments traité est moins de 50 fragments (temps d'exécution: 0.3 seconde)

    	donc on a decidé d'implimenter ce concept:

    	0. dans la partie creation de l'individu ( ou peut etre on va l'implimenter dans la mutation)...

    	0.1 on decoupe l'individu en des sous individus avec nombre de fragments = 50 (si sa taille est > 100)

    	1. executer pals sur toutes les sous-individus et recuperer les contigs.

		2. rassembler tous les sous-individus pour créer un individu.
		

5. aprés avoir fait ça sur metaheuristique orientation, faire la meme chose pour metaheuristique pour gagner du temps d'execution (qui peut etre signifique)

  - créer un 2 eme dictionaire qui contient les ids et les chaines en orientation inverse, puis créer les 4 dictionnaires de chevauchement
  - puis executer les memes etapes que precedent
    a. le fitness avec la fonction calcul chavauchement (max de 4 valeurs)

    b. crossover le meme

    c. mutation on decoupe l'individu -> execute pals -> rassemlber l'individu.

	


6. Remarque:
on a remarqué que quand on traite des grands fichiers, le calcule des chevauchements entre les fragments prends du temps parceque on etait en train de parcourir chaque fragment n-fois quand on calcule le chevauchement entre 2 fragments donc on a implimenté une nouvelle fonction qui:
  - prends les 10 derniers bases de fragment a gauche puis cherche les indices de leurs apparences dans le fragment a droite
  - si trouvé, on compare juste le reste, ça nous a gagner enormement du temps d'execution.

dans pals.py on fait ces changments:
1. get rid of "getorientation" fonction. // done
2. create dictionnaries for 4 chevauchements and use them in the function overlaps. // done
  2.1 get rid of Baseat() // done

7. in fitness:
  3.1 parcourir a partir de la fin et nn pas de debut et retourner dès qu'on trouve le chevauchement // pas la peine

  3.2 peut etre on a pas besoin d'utiliser la fonction overlaps,
  utiliser presque la meme fonction fitness que les metaheuristiques //Done

8. la création de nouveau crossover:
  1. Découpter les 2 individu en 3 (2 points de decoupage)
  2. aprés on swap la partie de milieu (p1 et p2)
  3. puis on sauvegarde le reste des 2 individu dans 2 listes
      3.1. on sauvegarde les indices des fragments qui sont présents dans le reste de parent1 et dans P2
      3.2. on fait la mm chose pour le reste de parent2 et P1

  4. puis on swap entre les fragments de ResteParent1 et ResteParent2
    qui ont les indices de l'etape précedente
