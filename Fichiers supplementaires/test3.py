import os.path, copy, time, random, glob
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO
from PALS import Pals
from dnaassemblyMAX import *
from Fonctions_Meta import *
from Dictionnaire_Orientation import *

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse

main_start_time = time.time()

filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
filename=filelist[1]
dict_new={}
sequences=[]
ids=[]
Fragments=[]
Fragments_Inverse=[]
for record in SeqIO.parse(filename, "fasta"):
    index = record.id.split('.')
    dict_new.update({(record.id): str(record.seq)})
    sequences.append(str(record.seq))
    ids.append(record.id)
    Frag_tmp=Fragment(record.id, str(record.seq), 1)
    Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(), 0)
    """on remplie les lists des fragments"""
    Fragments.append(Frag_tmp)
    Fragments_Inverse.append(Frag_tmp_inverse)

"""Les chevauchements avec: 
    L'orientation des fragments 'Left' est de gauche -> droite.
    L'orientation des fragments 'Right' est de gauche -> droite. """
D_suff_pref=Chevauch_Orientation(Fragments, Fragments)

"""Les chevauchements avec: 
    L'orientation des fragments 'Left' est de gauche -> droite. 
    L'orientation des fragments 'Right' est de droite -> gauche."""
D_suff_suff=Chevauch_Orientation(Fragments, Fragments_Inverse)

"""Les chevauchements avec: 
    L'orientation des fragments 'Left' est de droite -> gauche. 
    L'orientation des fragments 'Right' est de gauche -> droite."""
D_pref_pref=Chevauch_Orientation(Fragments_Inverse, Fragments)

"""Les chevauchements avec: 
    L'orientation des fragments 'Left' est de droite -> gauche. 
    L'orientation des fragments 'Right' est de droite -> gauche."""
D_pref_suff=Chevauch_Orientation(Fragments_Inverse, Fragments_Inverse)


def calcul_chevauch(frag_1, frag_2):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation
    Orientation=1 : de Gauche --> Droite
    Orientation=0 : de Droite --> Gauche"""
    if frag_1.Orientation==1 and frag_2.Orientation==1:
        return D_suff_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==1 and frag_2.Orientation==0:
        return D_suff_suff[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==0 and frag_2.Orientation==1:
        return D_pref_pref[frag_1.Id_Frag][frag_2.Id_Frag]

    if frag_1.Orientation==0 and frag_2.Orientation==0:
        return D_pref_suff[frag_1.Id_Frag][frag_2.Id_Frag]

def create_individu(): 
    sol=[]
    listFragsTmp=Fragments[:]
    for y in range(len(listFragsTmp)):
        val=random.choice(listFragsTmp)
        listFragsTmp.remove(val)
        val.Orientation=random.randint(0, 1)
        if val.Orientation==0:
            val.Seq_Frag=val.Inverse_Sequence()
        sol.append(val)

    return sol 

def inverse_orientation(Frag):
    Fragment=copy.deepcopy(Frag)
    Fragment.Seq_Frag= Fragment.Inverse_Sequence()
    if Fragment.Orientation==1:
        Fragment.Orientation=0
    else:
        Fragment.Orientation=1
    return Fragment

def swap_new(a, b):
    """ Fonction de permutation"""
    c=copy.deepcopy(a)
    d=copy.deepcopy(b)
    return inverse_orientation(d), inverse_orientation(c)

def mutation(individual):
    """ Swaper entre 2 fragments """
    
    #On choisi l'index de l'individu a muté aleatoirement
    mutate_index = random.randrange(len(individual))
    mutate_index2= mutate_index
    while(mutate_index==mutate_index2):
        mutate_index2 = random.randrange(len(individual))
    individual[mutate_index],individual[mutate_index2]=\
    swap_new(individual[mutate_index],individual[mutate_index2])

def crossover(parent1,parent2):
    """Croisement a un point des deux parents"""       
    l = len(parent1)
    a = int(l / 2)
    p1 = parent1[0:a]
    p1_ids= [element.Id_Frag for element in p1]
    p2 = parent2[a:l]
    choices=[]

    for i in range(len(parent2)):
        """les fragments dans parent2 et non pas dans p1"""
        if parent2[i].Id_Frag not in p1_ids:
             choices.append(parent2[i])
             
    i=0
    for j in range(len(p2)):
        """Verifier si la valeur existe dans le premier parent"""
        if p2[j].Id_Frag in p1_ids:
           p2[j]=choices[i]
           i=i+1

    child1 = p1 + p2
    child2 = p2 + p1

    return child1,child2

def Fitness_Chevauchements(individual): 
    """Fonction fitness qui maximise le nombre de chevauchement"""
    fitness = 0
    for i in range (len(individual)-1):
        fitness+=calcul_chevauch(individual[i], individual[i+1])
    return fitness

def efface_repetition_Orient(left, right):
    """ Effacer la partie repétée de 2 fragments qui se chevauchent"""
    for i in range(len(left)):
        if left[i:] == right[:len(left)-i]: 
            #on supprime toute la partie de left repeté dans right
            right=''.join(right.split(right[:len(left)-i]))
            return right
    return right

def genere_seq_Orient(contig):
    """Prends un contig et genere sa séquence equivalente"""
    sequence=contig[0].Seq_Frag       
    for i in range(len(contig)-1):
        sequence+= efface_repetition_Orient(contig[i].Seq_Frag,\
                                        contig[i+1].Seq_Frag)
    return sequence

def genere_seq_de_contig_Orient(contigs):
    """Genere la sequence equivalante d'une liste des contigs"""
    l=[]
    for contig in contigs:
        l.append(genere_seq_Orient(contig))
    return l

def meilleur_contig_Orient(contigs):
    """determine le meilleur contig a partir d'une liste des contigs"""
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    """on determine les contigs qui ont le plus grand nombre de fragments
    (car on peut avoir plusieurs contigs qui ont le même nombre de fragments)."""
    meilleurs_contigs=[f for f in contigs if len(f)==tailleBestContig]

    """après on genere les sequences pour ces contigs."""
    k=genere_seq_de_contig_Orient(meilleurs_contigs)
        
    """après on determine le meilleur contig en comparant la longueur 
    des sequences de ces contigs."""
    meilleur_contig= meilleurs_contigs[k.index(max(k,key=lambda x: len(x)))]  

    return meilleur_contig

def create_contigs_Orient(individual):
    """Genere les contigs a partir d'un individuel et retourne """
    cpt=0
    contigs=[[individual[0]]]
    for i in range (len (individual)-1):
        
        #10: Seuil. 
        if (calcul_chevauch(individual[i], individual[i+1]) < 10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(individual[i+1])

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(individual[i+1])  

    return contigs


def fitnessNbContig_Orient(individual, affich):
    """créer une liste des listes des fragments qui se chevauchent(contigs)"""
    contigs=create_contigs_Orient(individual)

    #determiner la taille de plus grand contig
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #Determiner le meilleur contig
    meilleur_contig_ = meilleur_contig_Orient(contigs)

    if affich:
        print("\nnombre de contigs final: ", len(contigs),\
        "\n\ntaille best contig: ", tailleBestContig,"\n\n")

    return len(contigs), tailleBestContig 

print("\nTemps d execution : %.2f secondes --" % (time.time() - main_start_time))    






"""
#Pour Pals on donne une liste qui contient les sequences et une liste qui contient leur ordre
Resultat=Pals(Gen_list_Seq(ordre),ordre)
print("\n",Resultat)
"""