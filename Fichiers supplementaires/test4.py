from dataclasses import dataclass
import os.path, copy, time, random, glob
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO

start_time = time.time()

@dataclass
class Fragment:
    Id_Frag: str
    Seq_Frag: str
    """Orientation est de gauche a droite si 1, et de droite a gauche si 0"""
    Orientation: bool=1

    def Inverse_Sequence(self):
        """Une fonction qui inverse la sequence adn d'un fragment"""
        Inverse=''.join(reversed(self.Seq_Frag))
        return Inverse


filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
filename=filelist[0]
dict_new={}
sequences=[]
ids=[]
Fragments=[]
Fragments_Inverse=[]
taille_ADN=0
counter=0
for record in SeqIO.parse(filename, "fasta"):
    index = record.id.split('.')
    text="frag"+'{0:04d}'.format(counter)
    dict_new.update({text: str(record.seq)})
    sequences.append(str(record.seq))
    ids.append(text)
    Frag_tmp=Fragment(text, str(record.seq), 1)
    Frag_tmp_inverse=Fragment(Frag_tmp.Id_Frag, Frag_tmp.Inverse_Sequence(), 0)
    """on remplie les lists des fragments"""
    Fragments.append(Frag_tmp)
    Fragments_Inverse.append(Frag_tmp_inverse)
    taille_ADN+=len(str(record.seq))
    counter+=1

print(ids)
print(dict_new)


""" 
IDS=Res[0]
Orientations=Res[1]
individu=[]

#on crée un individu qui contient les fragments recupérés a 
#partir du resultat de PALS, avec leurs orientations et dans l'ordre recupéré de PALS.
for i in range(len(ids)):
    tmp=Fragment(IDS[i], '', Orientations[i])
    individu.append(Create_Sequence(tmp))

test=fitnessNbContig_Orient(individu)
fit=Fitness_Chevauchements(individu)
"""
print("\nTemps d execution : %.2f secondes --" % (time.time() - start_time))    

