import os.path, copy, time, random, glob,sys
sys.path.append('./Modules')
sys.path.append('./PALS Python')
from Fonctions_pals import PALS
from tkinter import *
from pyeasyga import pyeasyga
from Dictionnaire import *
from Bio import SeqIO
#from PALS import Pals
from dnaassemblyMAX import *
from Fonctions_Meta import *

# Debut du decompte du temps
start_time = time.time()

"""Traitment de fichier de données"""
filelist=sorted(glob.glob('./Programme C/dna_instances_ordered/*.dat'))
#fileslim=filelist[2]
#fileslim=filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.dat"),("all files","*.*")))



def create_individu(data):
    """Creation de l'individu en aleatoire"""
    
    #Creation de l'individu en aleatoire
    rand_choix=random.randrange(0,150)
    if rand_choix>0:
        sol=[]
        listFragsTmp=[]
        for x in data:
            listFragsTmp.append(x)
        for y in range(len(data)):
            val=random.choice(listFragsTmp)
            listFragsTmp.remove(val)
            sol.append(val)
        return sol

    else:
        individual=data[:]
        random.shuffle(individual)
        Fragments=[]
        tmp_overlap={1:0, 0:0}
        for element in individual:
            #pour la liste des fragments
            Frag_tmp=Fragment(str(element), str(dict_new[element]),tmp_overlap, 1)
            Fragments.append(Frag_tmp)

        tmp=PALS(Fragments, len(individual), D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)[0]

        return tmp
    """elif rand_choix>145:
        #on initialise l'individu avec le resultat de l'heuristique
        individu=Res_heuristique[:]
        #ensuite on ajoute les fragments restant de manière aléatoire
        part_2=Rempli_Aleatoire(Rest)
        individu.extend(part_2)
        return individu 
    """

def calcul_chevauch(frag_1, frag_2):
    """Calcule le chevauchements entre 2 fragments dépendant sur leurs orientation"""
    number=[D_suff_pref[frag_1][frag_2], \
               D_suff_suff[frag_1][frag_2],\
               D_pref_pref[frag_1][frag_2],\
               D_pref_suff[frag_1][frag_2]]
    return max(number)

def fitMaxChevauchements( individual , data ): 
    """Fonction fitness qui maximise le nombre de chevauchement"""
    fitness = 0
    """for i in range (len(individual)-1):
        fitness+=calcul_chevauch(individual[i], individual[i+1])"""
    """fitness = len(dict_new[individual[0]])
    for i in range (len(individual)-1):
        tmp_fitt=calcul_chevauch(individual[i], individual[i+1])
        if tmp_fitt>=10:
            fitness+=len(dict_new[individual[i+1]])"""
    fitness = 0
    for i in range (len(individual)-1):
        tmp_fitt=calcul_chevauch(individual[i], individual[i+1])
        if tmp_fitt>=10:
            fitness+=1


    return fitness

def crossover(parent1,parent2):
    """Croisement a un point des deux parents"""       
    """l = len(parent1)
    a = int(l / 2)
    p1 = parent1[0:a]
    p2 = parent2[a:l]
    choices=[element for element in parent2[0:a] if element not in p1]
    indices=[p2.index(element) for element in p2 if element in p1]
    i=0                         
    for indice in indices:
        p2[indice]=choices[i]
        i=i+1
    """
    """Nouveau Crossover"""
    l = len(parent1)
    point1=random.randrange(0, int(l/2))
    point2=random.randrange(int(l/2),l+1)
    #Les parties de milieu
    p2=parent2[point1:point2]
    p1=parent1[point1:point2]
    #Les restes de P1 et P2
    ResteP1=parent1[0:point1]+parent1[point2:]
    ResteP2=parent2[0:point1]+parent2[point2:]
    indices1=[ResteP1.index(element) for element in ResteP1 if element in p2]
    indices2=[ResteP2.index(element) for element in ResteP2 if element in p1]
    i=0                         
    for indice in indices1:
        ResteP1[indice], ResteP2[indices2[i]]=ResteP2[indices2[i]],ResteP1[indice]
        i=i+1
    
    p11=ResteP1[0:point1]
    p12=ResteP1[point1:]
    p21=ResteP2[0:point1]
    p22=ResteP2[point1:]
    return p11 + p2 +p12, p21 + p1 + p22


def Decoupe_individual(individual):
    """Decoupe l'individuel en morceaux de 100 fragments et retourne 
    une liste qui contient les indices des morceaux
    exemple: taille : 350
            --> resultat [0,100,200,300,350]
    on va l'utiliser dans mutation pour executer pals sur ces petits morceaux"""
    resultat=[0]
    length=len(individual)
    nb_divisions, reste= divmod(length, 80)

    if nb_divisions>0:
        for i in range(1,nb_divisions+1):
            resultat.append(100*i)

    if reste >= 30:
        resultat.append(reste+resultat[-1]+1)
    elif reste>0:
        if len(resultat)>1:
            resultat[-1]+=reste+1
        else:
            resultat.append(reste+resultat[-1]+1)
    elif reste<=0:
            resultat[-1]+=1

    return resultat

def mutation(individual):
    """ Executer pals sur l'individu 
    || Swaper entre 2 fragments
    """
    """On choisi l'index de l'individu a muté aleatoirement"""
    rand_choix=random.randrange(0,10000)
    if rand_choix>0:
        mutate_index = random.randrange(len(individual))
        mutate_index2= mutate_index
        while(mutate_index==mutate_index2):
            mutate_index2 = random.randrange(len(individual))
        individual[mutate_index],individual[mutate_index2]=swap(individual[mutate_index],individual[mutate_index2])
    
    else:
        Fragments=[]
        tmp_overlap={1:0, 0:0}
        for element in individual:
            #pour la liste des fragments
            Frag_tmp=Fragment(element, str(dict_new[element]),tmp_overlap, 1)
            Fragments.append(Frag_tmp)

        tmp=PALS(Fragments, len(individual), D_suff_pref,D_suff_suff, D_pref_pref, D_pref_suff)[0]
        individual=tmp

def create_contigs(individual):
    """Genere les contigs a partir d'un individuel et retourne """
    cpt=0
    contigs=[[individual[0]]]
    for i in range (len(individual)-1):
        
        #10: Seuil. 
        if (calcul_chevauch(individual[i], individual[i+1]) < 10):
            cpt+=1 
            #si les fragments ne chevauchent pas, on crée un nouveau contig
            contigs.append([]) 
            #on ajoute le fragment i+1 au contig vide
            contigs[cpt].append(individual[i+1])

        else:
            #si les fragments se chevauchent, on ajoute le fragment au contig 
            contigs[cpt].append(individual[i+1])  

    return contigs

def fitnessNbContig(individual, d, affich):
    #créer une liste des listes des fragments qui se chevauchent(contigs)
    contigs=create_contigs(individual)

    #determiner la taille de plus grand contig
    tailleBestContig=len(sorted(contigs,key=lambda x: len(x))[-1])

    #Determiner le meilleur contig
    meilleur_contig_ = meilleur_contig(contigs,dict_new)

    if affich:
        print("\nnombre de contigs final: ", len(contigs),\
        "\n\ntaille best contig: ", tailleBestContig,"\n\n")

    return len(contigs), tailleBestContig 


def population_stats(informations_population):
    """Retourne: 1. Nombre de chevauchments, 2. nbr contigs, 3. taille de meilleur contig 
        pour chaque individu de la dernière generation"""
    infos_stats= []
    for element in informations_population:
        fit=fitnessNbContig(element[1],d,False)
        #on elimine les repetitions.
        if (element[0], fit[0], fit[1]) not in infos_stats:
            infos_stats.append((element[0],fit[0],fit[1]))
    return infos_stats

""" Programme principal"""

"""On execute l'algorithme genetic pour des differents nombres de generations et on analyse:"""
list_generations=[5]
#f = open("Resultats/1.Stats_Meta_Aleatoire.csv","a+")
#f = open("Resultats/2.Stats_Meta_Hybrid.csv","a+")

"""filelist contient les noms de fichiers adn en ordre croissant par nombre des fragments
ici, on parcour juste les 4 premiers fichiers qui sont les plus petits"""
for fileslim in filelist[0:1]:
    filename= os.path.split(fileslim)[1]
    dict_new={}
    dict_inverse={}
    records = list(SeqIO.parse(fileslim, "fasta"))
    data=[]
    taille_ADN=0
    counter=0
    for record in SeqIO.parse(fileslim, "fasta"):
        index = record.id.split('.')
        text="frag"+"{0:04d}".format(counter)   
        dict_new.update({str(text): str(record.seq)})
        Seq_Inverse=''.join(reversed(str(record.seq)))
        dict_inverse.update({(str(text)): Seq_Inverse})
        data.append(str(text))
        taille_ADN+=len(str(record.seq))
        counter=counter+1

    d = ChevechTousOLCGRE(dict_new)

    D_suff_pref=Chevauch_Orientation(dict_new, dict_new)
    D_suff_suff=Chevauch_Orientation(dict_new, dict_inverse)
    D_pref_pref=Chevauch_Orientation(dict_inverse, dict_new)
    D_pref_suff=Chevauch_Orientation(dict_inverse, dict_inverse)
    

    """
    On utilise le resultat de l'heuristique pour créer les individus et commencer avec 
    le plus grand contig puis les autres fragments on les rempli aléatoirment   """
    """temp_dict=copy.copy(d)  
    Res_heuristique=Heuristique(temp_dict,dict_new)

    #Affichage Des informations initiales 
    #(Le nombre des bases dans le fichier, nombre de fragments, 
    #et le meilleur contig qui est evidement 1):
    print("Resultat de l'heuristique: : ", len(Res_heuristique))
    """
    print("\nFichier: %s" % filename,
          "\nInfos initiales:\n\
            Taille totale des Bases: %d\n\
            Nombre de contigs: %d\n\
            Taille de meilleur contig: %d"% (taille_ADN,len(data),1))
     
    
    """Rest=[]
    for x in data:
            if x not in Res_heuristique:
                Rest.append(x)"""
    """
    f.write("\t\tFichier: %s\t\t\t\n"% filename)
    f.write("\n\tNombre de chevauchments, nombre de contigs, taille de meilleur contig")
    f.write("\nInformations Initiales: ,%s"% str(taille_ADN)+","+ str(len(data))+","+ str(1))
    """  
    for nb_gens in list_generations:
        temps_init=time.time()
        ga = pyeasyga.GeneticAlgorithm (data,
                                   population_size=800,
                                   generations=nb_gens,
                                   crossover_probability=0.8,
                                   mutation_probability=0.8,
                                   elitism=True,
                                   maximise_fitness=True) 
        ga.create_individual=create_individu
        ga.fitness_function=fitMaxChevauchements
        ga.crossover_function=crossover
        ga.mutate_function=mutation
        ga.selection=ga.tournament_selection 
        ga.run()
        best=ga.best_individual()
        last_gen=ga.last_generation()

        taille_chevauch=0
        for i in range(len(best[1])-1):
            taille_chevauch+=calcul_chevauch(best[1][i], best[1][i+1])



        #on recupère le tuple (nbr de chevauchments, individu) pour chaque individu de la population
        informations_population=infos_population(last_gen)
        Pop_stat=population_stats(informations_population)
        pourcentage= "%.2f%%" % (taille_chevauch*100/taille_ADN)
        fit=fitnessNbContig(best[1],d,0)

        print("\nMeilleur Individu:\n\
        Taille de chevauchement: %d\n\
        Nombre de contigs: %d\n\
        Taille de meilleur contig: %d"% (taille_chevauch, fit[0], fit[1]))
    
        print("\nPourcentage chevauché ac succés: ",pourcentage)
        


        #on recupère: [Nbr chevauchments, Nbr contigs, taillebestcontig] pour chaque individu
        #statistiques_population=population_stats(informations_population)
        
        temps_exec=time.time() - temps_init
        
        """#On rempli le fichier csv:
        #f.write("\t\tFichier: %s\t\t\t\n"% filename)
        f.write("\n\n\t\tNombre de generations: %d\t\t\t"% nb_gens)
        #f.write("\n\tNombre de chevauchments, nombre de contigs, taille de meilleur contig")
        #f.write("\nInformations Initiales: ,%s"% str(taille_ADN)+","+ str(len(ids))+","+ str(1))
        f.write("\nMeilleur individu:, %s" % (",".join((str(best[0]), str(fit[0]), str(fit[1])))))
        f.write("\nPourcentage chevauché: %s" % pourcentage)
        f.write("\nTemps d'execution: %.2f sec" % temps_exec)
    f.write("\n\n\n\n")
f.close()"""

# Affichage du temps d execution
print("\nTemps d execution : %.2f secondes ---" % (time.time() - start_time))    
